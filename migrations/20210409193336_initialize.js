exports.up = function(knex) {
  return knex.schema.createTable('users', (table) => {
          table.increments('user_id').unique();
          table.string('email').notNullable();
          table.boolean('email_confirmed').defaultTo(false);
          table.specificType('hashed_password', 'char(60)');
      })
      .then( ()=> {
          return knex.schema.createTable('sessions', (table) => {
              table.increments('session_id').unique();
              table.string('unique_token').unique();
              table.integer('user_id').references('user_id').inTable('users').notNullable();
              table.timestamp('session_start_time',6).defaultTo(knex.fn.now())
              table.timestamp('previous_callback_time',6).defaultTo(knex.fn.now());
          })
      })
      .then( () => {
          return knex.schema.createTable('google_auth', (table) => {
              table.increments('google_auth_id').unique();
              table.integer('user_id').references('user_id').inTable('users');
              table.string('access_token');
              table.string('refresh_token');
              table.string('scope');
              table.string('token_type');
              table.string('expiry_date');
          })
      })
      .then( () => {
          return knex.schema.createTable('leads', (table) => {
              table.increments('lead_id').unique();
              table.integer('user_id').references('user_id').inTable('users');
              table.string('street_address');
              table.string('city');
              table.string('state');
              table.string('zipcode');
              table.string('first_name');
              table.string('last_name');
              table.string('phone');
              table.string('email');
              table.string('spouse_name');
              table.string('lead_status');
              table.string('power_bill_amount');
              table.string('lead_notes');
              table.string('circle_back_reminder');
              table.boolean('circle_back_add_to_calendar');
              table.boolean('remind_me_location');
              table.boolean('ql_add_to_calendar');
              table.string('ql_appointment');
              table.string('ql_apt_invite');
              table.string('ni_reason');
              table.string('nq_reason');
              table.string('power_bill_upload');
              table.string('cordinates')
          })
      })
};

exports.down = function(knex) {
  return knex.schema.dropTable('leads')
  .then( ()=> {
        return knex.schema.dropTable('google_auth')
    })
  .then( ()=> {
      return knex.schema.dropTable('sessions')
  })
  .then( ()=> {
        return knex.schema.dropTable('users')
  })
};
