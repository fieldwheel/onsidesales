let map;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 27.935423999999998, lng: -82.4764939 },
    zoom: 19,
    mapTypeId: 'satellite',
    setMyLocationEnabled:true,
    mapTypeControl:false
    // disableDefaultUI: true
    // disableDefaultUI: true,
  });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(
          browserHasGeolocation
            ? "Error: The Geolocation service failed."
            : "Error: Your browser doesn't support geolocation."
        );
        infoWindow.open(map);
      }
