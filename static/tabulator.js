// Simple table
var tabledata = [
    {id:1, name:"Oli Bob", progress:12, gender:"male", rating:1, col:"red", dob:"19/02/1984", car:1},
    {id:2, name:"Mary May", progress:1, gender:"female", rating:2, col:"blue", dob:"14/05/1982", car:true},
    {id:3, name:"Christine Lobowski", progress:42, gender:"female", rating:0, col:"green", dob:"22/05/1982", car:"true"},
    {id:4, name:"Brendon Philips", progress:100, gender:"male", rating:1, col:"orange", dob:"01/08/1980"},
    {id:5, name:"Margret Marmajuke", progress:16, gender:"female", rating:5, col:"yellow", dob:"31/01/1999"},
    {id:6, name:"Frank Harbours", progress:38, gender:"male", rating:4, col:"red", dob:"12/05/1966", car:1},
];
//
// //initialize table
// var table = new Tabulator("#example-table", {
//     data:tabledata, //assign data to table
//     autoColumns:true, //create columns from data field names
// });


// Feature Rich table
// document
var printIcon = function(cell, formatterParams, onRendered){ //plain text value
   return "<image src='/images/edit.png' style='height:15px;width:15px;'>";
};

function substantiateTable() {
  // window.addEventListener('load', (event) => {
    // setTimeout(function(){
      var table = new Tabulator("#example-table", {
          data:tabledata,           //load row data from array
          layout:"fitColumns",      //fit columns to width of table
          // responsiveLayout:true,  //hide columns that dont fit on the table
          tooltips:true,            //show tool tips on cells
          addRowPos:"top",          //when adding a new row, add it to the top of the table
          virtualDomHoz:true,
          history:true,             //allow undo and redo actions on the table
          pagination:"local",       //paginate the data
          paginationSize:7,         //allow 7 rows per page of data
          movableColumns:true,      //allow column order to be changed
          resizableRows:true,       //allow row order to be changed
          initialSort:[             //set the initial sort order of the data
              {column:"name", dir:"asc"},
          ],
          columns:[                 //define the table columns
              {title:"Lead ID", field:"lead_id", editor:"input",width:100},
              {title:"Street Address", field:"street_address", editor:"input",width:100},
              {title:"City", field:"city", hozAlign:"left", formatter:"progress", editor:true,width:100},
              {title:"State", field:"state", width:95, editor:true},
              {title:"Zipcode", field:"zipcode", hozAlign:"center", width:100, editor:true},
              {title:"First name", field:"first_name", width:130, editor:"input"},
              {title:"Last name", field:"last_name", width:130, sorter:"date", hozAlign:"center"},
              {title:"Spouse", field:"spouse", width:90,  hozAlign:"center", formatter:"tickCross", sorter:"boolean", editor:true},
              {title:"Spouse name", field:"spouse_name", width:130, editor:"input"},
              {title:"Phone", field:"phone", width:90,  hozAlign:"center", editor:true},
              {title:"Email", field:"email", width:90,  hozAlign:"center", editor:true},
              {title:"Lead Status", field:"lead_status", width:90,  hozAlign:"center", editor:"select", editorParams:{
                "Select status":"Select status",
                "Not Home":"Not Home",
                "Not Interested":"Not Interested",
                "Not qualified":"Not qualified",
                "Circle back":"Circle back",
                "Qualified lead":"Qualified lead"}},
              {title:"Power bill amount", field:"power_bill_amount", formatter:"money", width:130, editor:"input"},
              {title:"Lead notes", field:"lead_notes", formatter:"textarea",width:130, editor:"input"},
              {title:"Set appointment", field:"set_appointment", formatter:"datetime", width:130, editor:"input"},
              {title:"Set reminder", field:"set_reminder", formatter:"datetime", width:130, editor:"input"},
              {title:"NQ Reason", field:"nq_reason", width:90,  hozAlign:"center", editor:"select", editorParams:{
                "Select status":"Select status",
                "Renting":"Renting",
                "Low credit":"Low credit",
                "Moving":"Moving",
                "Other":"Other",
              }},
              {title:"NI Reason", field:"nq_reason", width:90,  hozAlign:"center", editor:"select", editorParams:{
                "Select status":"Select status",
                "No soliciting":"No soliciting",
                "Didn't listen":"Didn't listen",
                "Not interested":"Not interested",
                "Already getting solar":"Already getting solar",
              }},
              {title:"Send calender reminder to prospective customer", field:"send_calender_reminder", width:90,  hozAlign:"center", formatter:"tickCross", sorter:"boolean", editor:true},
              {title:"Add to calender", field:"add_to_calender", width:90,  hozAlign:"center", formatter:"tickCross", sorter:"boolean", editor:true},
              {formatter:printIcon, width:40, hozAlign:"center", cellClick:function(e, cell){alert("Printing row data for: " + cell.getRow().getData().name)}},
          ],
      });
      table.hideColumn("lead_id")
      table.hideColumn("spouse")



    // }, 1000);

  // });
}

if(window.location.pathname == "/leads-list") {
  console.log("path name matched")
  listViewPageState();
  substantiateTable();
}
