// let body = buildFormInputsObj("application-form")
// console.log('body: ',body);
// console.log(applicationSubmitValidation(body))


// window.addEventListener('load', (event) => {
function GeocodeCoordinates (pos) {
    let applicationHeaders = new Headers();
    let appInit = {
        method: 'GET',
        headers: applicationHeaders,
    }
    let URL = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${pos}&key=AIzaSyAwMsvITYWopm-irb5-ntGIedwtGUVVn7I`;
    return fetch(URL, appInit).then(function(response){
        return response.json()
    })
    .then((res)=>{
      return res.results[0];
    })
    .catch( (error)=> {
      return error;
    })
}

function reverseGeocodeCoordinates(address) {
  console.log("entered address =", address)
  let addres = address.split(" ");
  address = [];
  addres.forEach((addres)=> {
    let values = addres.split(",");
    console.log("values=",values)
    address = address.concat(values)
    console.log("interum address=",address)
  });
  console.log("address=",address);
  address.join("%20");
  console.log("address=",address);
  "address=24%20Sussex%20Drive%20Ottawa%20ON"
  let applicationHeaders = new Headers();
  let appInit = {
      method: 'GET',
      headers: applicationHeaders,
  }
  let URL = `https://maps.googleapis.com/maps/api/geocode/json?address=address=${address}&key=AIzaSyAwMsvITYWopm-irb5-ntGIedwtGUVVn7I`;
  return fetch(URL, appInit).then(function(response){
      return response.json()
  })
  .then((res)=>{
    console.log("geocoding response 1=",res);
    if(res.results.length == 0) {
      alert("No results with address provided please check the address and attempt the search again.")
    }
    console.log("cordinates=",res.results[0].geometry.location);
    let cord = res.results[0].geometry.location;
    console.log("cord=",cord)
    positionMapToCordinates(cord,res.results[0])
    // return res.results[0];
  })
  .catch( (error)=> {
    return error;
  })
}

function uploadPowerBill (powerBill) {
  let loginHeaders = new Headers();
  loginHeaders.append('content-type', 'application/json');
  let appInit = {
      method: 'PUT',
      headers: loginHeaders,
      body: powerBill,
      keepalive:true
  };
  let loginURL = "/leads/power-bill-upload";
  console.log("appInit=",appInit)
  fetch(loginURL, appInit).then(function(response){
      console.log('sent lead update request')
      return response.json();
  })
  .then((lead)=>{
      console.log("retreived callback:",lead);
      updatedLeadPageState(lead,formObj);
  })
  .catch(function (error) {
      console.error("Fetch error:", error);
  });
}


  function updateLead(formObj) {
      console.log('Update lead function initiated')
      let body = formObj;
      let powerBill = (' ' + formObj.power_bill_upload).slice(1) ;
      console.log("update lead body=",body)
      console.log("power bill=",powerBill)
      delete body.power_bill_upload;
      let loginHeaders = new Headers();
      loginHeaders.append('content-type', 'application/json');

      // One thing I noticed when debugging was that the large string was lost when converting the object to json so I also tried converting the object to json manually below.
      // let JSONbody = Object
      //     .entries(body)
      //     .reduce((a, e) => {
      //       if (typeof e[1] != "function") {
      //         a += `"${e[0]}" : "${e[1]}", `;
      //       }
      //       return a;
      //     }, "`{")
      //     .slice(1, -2) + "}`";
      let appInit = {
          method: 'PUT',
          headers: loginHeaders,
          body: body,
          keepalive:true
      };
      let loginURL = "/leads";
      console.log("appInit=",appInit)
      fetch(loginURL, appInit).then(function(response){
          console.log('sent lead update request')
          return response.json();
      })
      .then((lead)=>{
          console.log("retreived callback:",lead);
          uploadPowerBill(powerBill);
          updatedLeadPageState(lead,formObj);
      })
      .catch(function (error) {
          console.error("Fetch error:", error);
      });
  }


  function submitLead(formObj) {
    var infowindow = new google.maps.InfoWindow({
      content: "",
    });

      console.log('submitLeadFunctionInitiated')
      let body = formObj;
      console.log("submitLead body = ",body)
      let loginHeaders = new Headers();
      loginHeaders.append('content-type', 'application/json');
      let appInit = {
          method: 'POST',
          headers: loginHeaders,
          body: JSON.stringify(body)
      };
      let loginURL = "/leads";
      fetch(loginURL, appInit).then(function(response){
          console.log('sent lead submit request')
          return response.json();
      })
      .then((lead)=>{
          console.log("retreived callback:",lead);
          globalObj.currentMarker[0].setMap(null);
          let cord = formObj.cordinates.split(",");
          let latLng = {lat: parseFloat(cord[0]), lng: parseFloat(cord[1])};
          console.log("latLng=",latLng);
          let svgMarker;
          // console.log(lead)
          if(lead.lead_status == "select status") {
            svgMarker = svgMarkerSelect;
          }
          else if(lead.lead_status == "Not interested" ){
            console.log("equalerd Not Interested")
            svgMarker = svgMarkerNI;
          }
          else if(lead.lead_status == "Not home" ){
            svgMarker = svgMarkerNH;
          }
          else if(lead.lead_status == "Not qualified" ){
            svgMarker = svgMarkerNQ;
          }
          else if(lead.lead_status == "Circle back" ){
            svgMarker = svgMarkerCB;
          }
          else if(lead.lead_status == "Qualified lead" ){
            svgMarker = svgMarkerQL;
          }
          const marker = new google.maps.Marker({
             position: latLng,
             map: map,
             title: lead.lead_id.toString(),
             icon:svgMarker
           });
           formObj.lead_id = lead.lead_id;
           infowindow = new google.maps.InfoWindow({
             content: existingLeadInfoWindow(formObj),
           });
           google.maps.event.addListener(marker, 'click', function() {
             console.log("clicked existing lead")
             if (globalObj.currentMarker[0]) {
               globalObj.currentMarker[0].setMap(null)
             }
              infowindow.open(map,marker);
              if(screen.width < 650) {
                changeState("sales-form-mobile");
              }
              document.getElementById("status-filters").hidden = true;
              globalObj.formModal = true;
              document.getElementById("sales-form-mobile").innerHTML = existingLeadInfoWindow(formObj);
              globalObj.currentInfowindow[0] = infowindow;
              globalObj.currentMarker[0] = marker;
              setTimeout(function(){
                existingLeadInfoWindowFormatting(lead);
                Array.from(document.getElementsByClassName("edit-lead")).forEach((element)=> {
                  // setTimeout(function(){
                    // console.log("adding event listener to existing lead")
                    console.log("element =", element)
                    element.addEventListener("click",(event)=> {
                      console.log("clicked element")
                      editExistingLeadInfoWindowFormatting()
                    })

                 // }, 500);

                })
                Array.from(document.getElementsByClassName("save-lead-button")).forEach((element)=> {
                  element.addEventListener("click", (event) => {
                    event.preventDefault();
                    console.log("clicked save-lead-button");
                    updateExistingLead()
                  })
                })

                mirrorFormsInputs() }, 1000);
            });
            changeState("sales-page")
            return false

      })
      .catch(function (error) {
          console.error("Fetch error:", error);
      });
  }


  function getLeads() {
    // var infowindow = new google.maps.InfoWindow({
    //   content: "",
    // });
    // console.log("executing get leads")
    let leadHeaders = new Headers();
    leadHeaders.append('content-type', 'application/json');
    let appInit = {
        method: 'GET',
        headers: leadHeaders,
    };
    let loginURL = "/leads";
    fetch(loginURL, appInit).then(function(response){
        console.log('sent getLeads request')
        return response.json();
    })
    .then((leads)=>{
        console.log("leads=",leads);
        if(leads.message == "not logged in"){
          return homePageState()
        }
        setMarkersForExistingLeads(leads);
        globalObj.leads = leads;
    })
    .catch(function (error) {
        console.error("Fetch error:", error);
        console.log("Fetch error:", error);
    });
  }




// });
