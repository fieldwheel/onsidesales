/* Add path and state_function to be triggered below (triggered on Page Load and back/forward actions of browser). Include STATIC ROUTES before dynamic routes(except parameter).
Do not inlcude paramters when specifying state_function in the router's object. Just include the stateFuntion() and the router will pass the value of the URL parameter stateFunction(parameterName) for DYNAMIC ROUTES on its own.
*/
const pageLoadOptimizer = false;
const router = [
  // STATIC ROUTES
  {
    path:'/',
    state_function:"homePageState()"
  },
  //DYNAMIC ROUTES
  {
    path: "/sales-map",
    state_function:"salesMapPageState()",
  },
  {
    path: "/leads-list",
    state_function:"listViewPageState()",
  },
  {
    path:"/settings",
    state_function:"settingsPageState()"
  }
]

document.addEventListener('DOMContentLoaded', function() {

//BEGIN OF ADD ANY FUNCTIONS YOU WANT TO RUN ON PAGE LOAD HERE
        // userLoggedInValidation();
// END OF CUSTOM PAGE LOAD FUNCTIONS START OF FRAMEWORK FUNCTIONS

// START OF ROUTER CODE
  var path = window.location.pathname;
  pathArray = path.split("/");
  if(path[path.length-1]=="/"){
    path = path.slice(0,path.length-1)
  }
  for(let i=0;i<router.length;i++) {
    // remove any trailing /'s
    let routerPathArray = router[i].path.split("/");
    // console.log("routerPathArray",routerPathArray)
    let pathVariables = [];
    let executable = router[i].state_function.substring(0,router[i].state_function.length-1);
    if(pathArray.length === routerPathArray.length) {
      // console.log("paths equalled at pathArray=",pathArray)
      // console.log("paths equalled at routerPathArray=",routerPathArray)
      for(let j = 0; j<pathArray.length; j++) {
        if(pathArray[j] == routerPathArray[j]) {
          if(j == pathArray.length-1) {
            if(pathVariables) {
              // console.log("Path variables:",pathVariables)
              for(let k = 0; k<pathVariables.length;k++) {
                executable = executable + `'${pathVariables[k]}'`+","
                if(k==pathVariables.length-1) {
                  executable = executable.substring(0,executable.length-1) + ")";
                  // console.log("returning executable1=",executable);
                  return eval(executable);
                }
              }
            }
            return eval(router[i].state_function)
          }
          else {
            continue
          }
        }
        else {
          if(routerPathArray[j].includes(":")) {
            // console.log("path array includes :")
            pathVariables.push(pathArray[j]);
            if(j==pathArray.length-1) {
              for(let k = 0; k<pathVariables.length;k++) {
                // console.log("Path variables:",pathVariables)
                executable = executable + '"' + pathVariables[k]+ '"'+",";
                // console.log("execuatable prior:",executable)
                if(k==pathVariables.length-1) {
                  executable = executable.substring(0,executable.length-1)+ ")";
                  // console.log("returning : executable2=",executable);
                  return eval(executable);
                }
              }
            }
            else {
              continue
            }
          }
          break
        }
      }
      }
    }
});

window.addEventListener('popstate', function() {
    let path = window.location.pathname;
    pathArray = path.split("/");
    for(let i=0;i<router.length;i++) {
      // remove any trailing /'s
      if(path[path.length-1]=="/"){
        path = path.slice(0,path.length-1)
      }
      let routerPathArray = router[i].path.split("/");
      let pathVariables = [];
      let executable = router[i].state_function.substring(0,router[i].state_function.length-1);
      if(pathArray.length == routerPathArray.length) {
        for(let j = 0; j<pathArray.length; j++) {
          if(pathArray[j] == routerPathArray[j]) {
            if(j == pathArray.length-1) {
              for(let k = 0; k<pathVariables.length;k++) {
                executable = executable + `'${pathVariables[k]}'`+","
                if(k==pathVariables.length-1) {
                  executable = executable.substring(0,executable.length-1) + ")";
                  // console.log("returning executable=",executable);
                  return eval(executable);
                }
              }
              return eval(router[i].state_function)
            }
            else {
              continue
            }
          }
          else {
            if(routerPathArray[j].includes(":")) {
              pathVariables.push(pathArray[j]);
              if(j==pathArray.length-1) {
                for(let k = 0; k<pathVariables.length;k++) {
                  executable = executable + pathVariables[k]+",";
                  if(k==pathVariables.length-1) {
                    executable = router[i].state_function.substring(0,router[i].state_function.length-1) + ")";
                    return eval(executable);
                  }
                }
              }
              else {
                continue
              }
            }
            break
          }
        }
        }
      }
});

// END OF ROUTER CODE
