const containersObject = {
  "home-page":"home-page",
  "sales-page":"sales-page",
  "list-page":"list-page",
  "sales-form-mobile":"sales-form-mobile",
  "settings-page":"settings-page"
}


function changeState(container1, container2, container3, container4, container5) {
  for (let i in containersObject) {
    if(i != container1 || i != container2 || i != container3 || i != container4 || i != container5) {
      // console.log("element container:",i)
      // console.log("element container element:",document.getElementById(i))
      document.getElementById(i).hidden = true;
    }
  }
  if(container1) {
    document.getElementById(container1).hidden = false;
  }
  if(container2) {
    document.getElementById(container2).hidden = false;
  }
  if(container3) {
    document.getElementById(container3).hidden = false;
  }
  if(container4) {
    document.getElementById(container4).hidden = false;
  }
  if(container5) {
    document.getElementById(container5).hidden = false;
  }
  // globalObj.pageState = [container1, container2, container3, container4, container5];
}


function homePageState() {
  changeState("home-page")
}

function salesMapPageState() {
  changeState("sales-page")
}

function settingsPageState() {
  changeState("settings-page");
  console.log("clicked settings")
}

function loginWithGoogle() {
  console.log("intitated loginWithGoogle()")
  let GoogleLoginHeaders = new Headers();
  GoogleLoginHeaders.append('content-type', 'application/json');
  let appInit = {
      method: 'GET',
      credentials: 'same-origin',
      headers: GoogleLoginHeaders,
  }
  let URL = `user/google-auth-login`;
  fetch(URL, appInit).then(function(response){
      return response.json()
  })
  .then((res)=>{
      console.log("url: ", res.url_redirect)
      location.assign(res.url_redirect)
  })
  .catch( (error)=> {
      console.log(error);
  })
}

function leadFormSubmit(event) {
  event.preventDefault()
  let formObj = buildFormInputsObj("lead-form");
  formObj.cordinates = document.querySelector("form[cord]").getAttribute("cord");
  console.log("cord=",formObj.cordinates)
  console.log("formObj = ",formObj)
  return submitLead(formObj);
}

function existingLeadInfoWindowFormatting(lead) {
  console.log("lead for existing lead inofwindow forming=",lead)
  // document.getElementById("spouseNameBooleen-map").hidden = true;
  Array.from(document.getElementsByClassName("spouseName")).forEach((element)=> {
    if(element.value.length) {
      console.log("found spouse name")
      element.hidden = false;
      document["lead-form"][1].spouse.disabled = false;
      document["lead-form"][1].spouse.value = "yes";
      document["lead-form"][1].spouse.disabled = true;
      document["lead-form"][0].spouse.disabled = false;
      document["lead-form"][0].spouse.value = "yes";
      document["lead-form"][0].spouse.disabled = true;
    }
  })
  console.log("the next lead is forming")
  Array.from(document.getElementsByClassName("lead-actions")).forEach((element)=> {
    console.log("lead action value=",element.value)
    element.value = lead.lead_status
  })
  if(document["lead-form"][0].lead_status.value == "Not interested") {
    Array.from(document.getElementsByClassName("ni-actions-map")).forEach((element)=> {
      element.hidden = false;
    })
  }
  if(document["lead-form"][0].lead_status.value == "Not qualified") {
    Array.from(document.getElementsByClassName("nq-actions-map")).forEach((element)=> {
      element.hidden = false;
    })
  }
  if(document["lead-form"][0].lead_status.value == "Circle back") {
    Array.from(document.getElementsByClassName("cb-actions-map")).forEach((element)=> {
      element.hidden = false;
    })
  }
  if(document["lead-form"][0].lead_status.value == "Qualified lead") {
    Array.from(document.getElementsByClassName("ql-actions-map")).forEach((element)=> {
      element.hidden = false;
    })
  }
  Array.from(document.getElementsByClassName("ni_reason")).forEach((element)=> {
    element.value = element.getAttribute("value");
  })
  Array.from(document.getElementsByClassName("nq_reason")).forEach((element)=> {
    element.value = element.getAttribute("value");
  })
}

function editExistingLeadInfoWindowFormatting() {
  Array.from(document.getElementsByClassName("edit-lead")).forEach((element)=> {
    element.hidden = true;
  })
  enableFormInputs("lead-form");
  console.log("tiggered formatting");
  Array.from(document.getElementsByClassName("spouseNameBooleen")).forEach((element)=> {
    element.hidden = false;
  })
  Array.from(document.getElementsByClassName("save-lead-button")).forEach((element)=> {
    element.hidden = false;
  })
  if(document["lead-form"][0].spouse_name.length) {
    Array.from(document.getElementsByClassName("spouseName")).forEach((element) => {
      element.hidden = false;
    });
    document["lead-form"][0].spouse.value = "yes";
    document["lead-form"][1].spouse.value = "yes";
  }
  leadFormConditionalInputsEL();
  // document.getElementById("save-lead-button").addEventListener("click",(event)=> {
  //   event.preventDefault();
  //
  // })
}

function closePriorInfoWindow() {
  console.log("executing close prior info window");
  if (globalObj.currentMarker[0]) {
    console.log(globalObj.currentMarker[0].title)
    globalObj.currentInfowindow[0].close(map,globalObj.currentMarker[0])
  }
  if (globalObj.currentMarker[0] && globalObj.currentMarker[0].title == "click location") {
    globalObj.currentMarker[0].setMap(null)
  }
}

function updateExistingLead() {
  event.preventDefault()
  let formObj = buildFormInputsObj("lead-form");
  formObj.cordinates = document.querySelector("form[cord]").getAttribute("cord");
  formObj.lead_id = document.querySelector("form[lead_id]").getAttribute("lead_id");
  console.log("lead_id=",formObj.lead_id);
  console.log("formObj = ",formObj)
  updateLead(formObj);
}

function listViewPageState() {
  changeState("list-page")
  if(!document.getElementById("example-table")){
    document.getElementById("list-page").insertAdjacentHTML("beforeend",`<div style="margin-left:15px;margin-right:15px"id="example-table"></div>`)
    substantiateTable();
  }

}

function leadFormConditionalInputsEL() {
  // console.log("running leadFormCOnistionalInputEL function")
  Array.from(document.getElementsByClassName("spouseNameBooleen")).forEach((element)=> {
    element.addEventListener("click", (event)=> {
      console.log("confirm element", event.currentTarget.children[3])
      if(event.currentTarget.children[3].checked == true) {
        console.log("clicked yes")
        Array.from(document.getElementsByClassName("spouseName")).forEach((element)=> {
          element.hidden = false;
        })
      }
      else {
        Array.from(document.getElementsByClassName("spouseName")).forEach((element)=> {
          element.hidden = true;
        })
      }
  })
})
  Array.from(document.getElementsByClassName("lead-actions")).forEach((element)=> {
    element.addEventListener("change",(event)=> {
      // yourSelect.options[ yourSelect.selectedIndex ].value
      console.log(event.currentTarget.options[event.currentTarget.selectedIndex].value);
      if(event.currentTarget.options[event.currentTarget.selectedIndex].value == "Not interested") {
        console.log("selected not interested")
        Array.from(document.getElementsByClassName("ni-actions-map")).forEach((element)=> {
          element.hidden = false;
        })
        Array.from(document.getElementsByClassName("nq-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("cb-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("ql-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
      }
      else if (event.currentTarget.options[event.currentTarget.selectedIndex].value == "Not qualified") {
        Array.from(document.getElementsByClassName("ni-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("nq-actions-map")).forEach((element)=> {
          element.hidden = false;
        })
        Array.from(document.getElementsByClassName("cb-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("ql-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
      }
      else if (event.currentTarget.options[event.currentTarget.selectedIndex].value == "Circle back") {
        Array.from(document.getElementsByClassName("ni-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("nq-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("cb-actions-map")).forEach((element)=> {
          element.hidden = false;
        })
        Array.from(document.getElementsByClassName("ql-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
      }
      else if (event.currentTarget.options[event.currentTarget.selectedIndex].value == "Qualified lead") {
        Array.from(document.getElementsByClassName("ni-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("nq-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("cb-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("ql-actions-map")).forEach((element)=> {
          element.hidden = false;
        })
      }
      else if (event.currentTarget.options[event.currentTarget.selectedIndex].value == "Not home") {
        Array.from(document.getElementsByClassName("ni-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("nq-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("cb-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
        Array.from(document.getElementsByClassName("ql-actions-map")).forEach((element)=> {
          element.hidden = true;
        })
      }
    })

  })
}

function resizeFunction(template) {
  console.log("resized screen size = ", screen.width);
  if(screen.width < 650 && globalObj.formModal && window.location.pathname.includes("sales-map")) {
    console.log("evaluated to true")
    document.getElementById("sales-form-mobile").hidden = false;
    changeState("sales-form-mobile");
  }
  else if(!window.location.pathname.includes("leads-list")){
    changeState("sales-page");
  }
}

function updatedLeadPageState(lead,formObj) {
  globalObj.currentMarker[0].setMap(null);
  console.log("lead=",lead)
  let cord = lead.cordinates.split(",");
  let latLng = {lat: parseFloat(cord[0]), lng: parseFloat(cord[1])};
  console.log("latLng=",latLng);
  let svgMarker;
  // console.log(lead)
  if(lead.lead_status == "select status") {
    svgMarker = svgMarkerSelect;
  }
  else if(lead.lead_status == "Not interested" ){
    console.log("equalerd Not Interested")
    svgMarker = svgMarkerNI;
  }
  else if(lead.lead_status == "Not home" ){
    svgMarker = svgMarkerNH;
  }
  else if(lead.lead_status == "Not qualified" ){
    svgMarker = svgMarkerNQ;
  }
  else if(lead.lead_status == "Circle back" ){
    svgMarker = svgMarkerCB;
  }
  else if(lead.lead_status == "Qualified lead" ){
    svgMarker = svgMarkerQL;
  }
  const marker = new google.maps.Marker({
     position: latLng,
     map: map,
     title: lead.lead_id.toString(),
     icon:svgMarker
   });
   let infowindow = new google.maps.InfoWindow({
     content: existingLeadInfoWindow(lead),
   });
   console.log(marker);
   addMarkerEventListener(marker,infowindow,lead,formObj);
   changeState("sales-page");
}

function setMarkersForExistingLeads(leads) {
  leads.forEach((lead)=> {
    // console.log("cords=",lead.cordinates);
    let cord = lead.cordinates.split(",");
    let latLng = {lat: parseFloat(cord[0]), lng: parseFloat(cord[1])};
    // console.log("latLng=",latLng);
    let svgMarker;
    console.log(lead)
    if(lead.lead_status == "select status") {
      svgMarker = svgMarkerSelect;
    }
    else if(lead.lead_status == "Not interested" ){
      console.log("equalerd Not Interested")
      svgMarker = svgMarkerNI;
    }
    else if(lead.lead_status == "Not home" ){
      svgMarker = svgMarkerNH;
    }
    else if(lead.lead_status == "Not qualified" ){
      svgMarker = svgMarkerNQ;
    }
    else if(lead.lead_status == "Circle back" ){
      svgMarker = svgMarkerCB;
    }
    else if(lead.lead_status == "Qualified lead" ){
      svgMarker = svgMarkerQL;
    }
    const marker = new google.maps.Marker({
       position: latLng,
       map: map,
       title: lead.lead_id.toString(),
       icon:svgMarker,
       lead_status: lead.lead_status
     });
     globalObj.markers.push(marker);
     google.maps.event.addListener(marker, 'click', function() {
       var infowindow = new google.maps.InfoWindow({
         content: existingLeadInfoWindow(lead),
       });
       console.log("lead=",lead);
      closePriorInfoWindow();
       setTimeout(function(){
         infowindow.open(map,marker);
         if(screen.width < 650) {
           changeState("sales-form-mobile")
         }
         document.getElementById("status-filters").hidden = true;
         globalObj.formModal = true;
         document.getElementById("sales-form-mobile").innerHTML = existingLeadInfoWindow(lead);
         // document["lead-form"][0].setAttribute("name","lead-form-mobile");
         globalObj.currentInfowindow[0] = infowindow;
         globalObj.currentMarker[0] = marker;
         // infowindow.close(map,globalObj.currentMarker[0])

         setTimeout(function(){
           existingLeadInfoWindowFormatting(lead);
           Array.from(document.getElementsByClassName("edit-lead")).forEach((element)=> {
             // setTimeout(function(){
               // console.log("adding event listener to existing lead")
               console.log("element =", element)
               element.addEventListener("click",(event)=> {
                 console.log("clicked element")
                 editExistingLeadInfoWindowFormatting()
               })

            // }, 500);

           })
           Array.from(document.getElementsByClassName("save-lead-button")).forEach((element)=> {
             element.addEventListener("click", (event) => {
               event.preventDefault();
               console.log("clicked save-lead-button");
               updateExistingLead();
             })
           })


           mirrorFormsInputs() }, 1000);





       },0);

      });
  })
}

function positionMapToCordinates(cord,result) {
  map.setCenter(new google.maps.LatLng(cord));
  const marker = new google.maps.Marker({
     position: cord,
     map: map,
     title: "searched location",
     icon:svgMarkerNew
   });
   const infowindow = new google.maps.InfoWindow({
     content: addLeadInfoWindow(result,cord),
   });
   globalObj.currentMarker[0] = marker;
   globalObj.currentInfowindow[0] = infowindow;
   setTimeout(function(){
     leadFormConditionalInputsEL();
     mirrorFormsInputs()
     Array.from(document.getElementsByClassName("save-lead-button")).forEach((element) => {
       // element.setAttribute("onclick", "leadFormSubmit()");
       element.addEventListener("click", (event) => {
         event.preventDefault();
         console.log("clicked save-lead-button");
         leadFormSubmit(event);
       })
     })
   }, 500)

   google.maps.event.addListener(infowindow,'closeclick',function(){
     // currentMark.setMap(null); //removes the marker
     console.log("closed info window")
     let markers = globalObj.currentMarker;
     function setMapOnAll(map) {
       for (let i = 0; i < markers.length; i++) {
         markers[i].setMap(map);
       }
     }
     // Removes the markers from the map, but keeps them in the array.
     function clearMarkers() {
       setMapOnAll(null);
     }
     // Shows any markers currently in the array.
     function showMarkers() {
       setMapOnAll(map);
     }
     (function deleteMarkers() {
       clearMarkers();
       markers = globalObj.currentMarker;
     })()
   // then, remove the infowindows name from the array
   });
   // function change() {
    return new Promise(function(resolve, reject) {
      // existingLeadInfoWindowFormatting(lead);
      infowindow.open(map, marker);
      if(screen.width < 650) {
        changeState("sales-form-mobile");
      }
      globalObj.formModal = true;
      let pos = cord.lat + "," + cord.lng;
      document.getElementById("sales-form-mobile").innerHTML = addLeadInfoWindow(result,pos);
      globalObj.currentInfowindow[0] = infowindow;
      // marker.addListener("click", () => {
      //      infowindow.open(map, marker)
      //      globalObj.currentInfowindow[0] = infowindow;
      //  })

       // setTimeout(resolve, 500)
      })

}

function mapPageState() {
  changeState("sales-page");
}
