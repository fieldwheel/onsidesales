function buildFormInputsObj (formName) {
    let formObject = {}
    let formElements;
    if(document[`${formName}`][0]){
      formElements = Array.from(document[`${formName}`][1].querySelectorAll('*'))
    }
    else {
      formElements = Array.from(document[`${formName}`].querySelectorAll('*'))
    }
    formElements.forEach(element => {
        if(element.localName === "input") {
            if(element.type === "checkbox") {
                if(element.parentElement.attributes.elementNumber) {
                    if(formObject[element.parentElement.attributes.name.value]) {
                        if(!formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]) {
                            formObject[element.parentElement.attributes.name.value].push({})
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)][element.name] = element.checked;
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]["element_sequence_number"] = element.parentElement.attributes.elementNumber.value;
                        }
                        else {
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)][element.name] = element.checked;
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]["element_sequence_number"] = element.parentElement.attributes.elementNumber.value;
                        }
                    }
                    else {
                        formObject[element.parentElement.attributes.name.value] = [];
                        formObject[element.parentElement.attributes.name.value].push({})
                    }
                }
                else {
                    formObject[element.name] = element.checked;
                }
            }
            else if (element.type === "file") {
              console.log("found input type of file");
              // let blob;
              // let file = getByteArray(element.files[0]).then((f)=> {
              //   const blob = new Blob(f);
              //   return blob
              // }).then((b)=> {
              //   blob = b;
              // })
              // formObject[element.name] = fileToBinaryString(blob);
              // console.log("element file name=",fileToBinaryString(blob));
              var file = element.files[0] // get handle to local file.
              var reader = new FileReader();
              reader.onload = function(event) {
                  var data = event.target.result;
                  console.log(ArrayBufferToBinary(data).toString());
                  formObject[element.name] = ArrayBufferToBinary(data).toString();
              };
              reader.readAsArrayBuffer(file); //gets an ArrayBuffer of the file
              // formObject[element.name] = fileToBinaryString(blob);


              // var myBlob = new Blob("usvString",{type:`${}`});
              // var myFile = blobToFile(myBlob, element.value);

               // reader.readAsArrayBuffer(element.files[0]);
            }
            else {
                if(element.parentElement.attributes.elementNumber) {
                    if(formObject[element.parentElement.attributes.name.value]) {
                        if(!formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]) {
                            formObject[element.parentElement.attributes.name.value].push({})
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)][element.name] = element.value;
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]["element_sequence_number"] = element.parentElement.attributes.elementNumber.value;
                        }
                        else {
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)][element.name] = element.value;
                            formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]["element_sequence_number"] = element.parentElement.attributes.elementNumber.value;
                        }
                    }
                    else {
                        formObject[element.parentElement.attributes.name.value] = [];
                        formObject[element.parentElement.attributes.name.value].push({})
                    }
                }
                else {
                    formObject[element.name] = element.value;
                }
            }
        }
        if(element.localName === "textarea" || element.localName === "datalist" || element.localName === "output" ) {
            if(element.parentElement.attributes.elementNumber) {
                if(formObject[element.parentElement.attributes.name.value]) {
                    if(!formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]) {
                        formObject[element.parentElement.attributes.name.value].push({})
                        formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)][element.attributes.name.value] = element.value;
                        formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]["element_sequence_number"] = element.parentElement.attributes.elementNumber.value;
                    }
                    else {
                        formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)][element.attributes.name.value] = element.value;
                        formObject[element.parentElement.attributes.name.value][Number.parseInt(element.parentElement.attributes.elementNumber.value)]["element_sequence_number"] = element.parentElement.attributes.elementNumber.value;
                    }
                }
                else {
                    formObject[element.parentElement.attributes.name.value] = [];
                    formObject[element.parentElement.attributes.name.value].push({})
                }
            }
            else {
                formObject[element.attributes.name.value] = element.value;
            }
        }
        if(element.localName === "option") {
            if(element.parentElement.parentElement.attributes.elementNumber) {
                if (formObject[element.parentElement.parentElement.attributes.name.value]) {
                    if(!formObject[element.parentElement.parentElement.attributes.name.value][Number.parseInt(element.parentElement.parentElement.attributes.elementNumber.value)]){
                        formObject[element.parentElement.parentElement.attributes.name.value].push({})
                        formObject[element.parentElement.parentElement.attributes.name.value][Number.parseInt(element.parentElement.parentElement.attributes.elementNumber.value)][`${element.parentElement.attributes.name.value}`] = element.parentElement.selectedOptions[0].text
                        formObject[element.parentElement.parentElement.attributes.name.value][Number.parseInt(element.parentElement.parentElement.attributes.elementNumber.value)]['element_sequence_number'] = element.parentElement.parentElement.attributes.elementNumber.value;
                    }
                    else {
                        formObject[element.parentElement.parentElement.attributes.name.value][Number.parseInt(element.parentElement.parentElement.attributes.elementNumber.value)][`${element.parentElement.attributes.name.value}`] = element.parentElement.selectedOptions[0].text
                        formObject[element.parentElement.parentElement.attributes.name.value][Number.parseInt(element.parentElement.parentElement.attributes.elementNumber.value)]['element_sequence_number'] = element.parentElement.parentElement.attributes.elementNumber.value;
                    }
                }
                else {
                    formObject[element.parentElement.parentElement.attributes.name.value] = [];
                    formObject[element.parentElement.parentElement.attributes.name.value].push({})
                }
            }
            else {
                formObject[element.parentElement.attributes.name.value] = element.parentElement.selectedOptions[0].text;
            }
        }
    })
    return formObject
}

function enableFormInputs(formName) {
    // if(formName.length) {
      console.log("formName=",formName)
      // Array.from(formName).forEach((form,i)=> {
      let formObject = {};
      let formElements = Array.from(document[`${formName}`][0].querySelectorAll('*'));
      formElements.forEach(element => {
          if(element.localName === "input" || element.localName === "textarea" || element.localName === "datalist" || element.localName === "output" || element.localName === "select" || element.localName === "file" ) {
            element.disabled = false;
          }
      })
      let formElements2 = Array.from(document[`${formName}`][1].querySelectorAll('*'));
      formElements2.forEach(element => {
          if(element.localName === "input" || element.localName === "textarea" || element.localName === "datalist" || element.localName === "output" || element.localName === "select" || element.localName === "file") {
            element.disabled = false;
          }
      })
}

function disableFormInputs(formName) {
    let formObject = {};
    let formElements = Array.from(document[`${formName}`].querySelectorAll('*'));
    formElements.forEach(element => {
        if(element.localName === "input" || element.localName === "textarea" || element.localName === "datalist" || element.localName === "output" || element.localName === "select" || element.localName === "file") {
          element.disabled = true;
        }
    })
}


function clearFormInputsObj (formName) {
  let formElements = Array.from(document[`${formName}`].querySelectorAll('*'))
  formElements.forEach(element => {
      if(element.localName === "input") {
          // console.log(element);
          if(element.type === "checkbox") {
            element.checked = false;
          }
          else {
            element.value = ""
          }
      }
      if(element.localName === "textarea" || element.localName === "datalist" || element.localName === "output" ) {
          // console.log(element);
          element.value = "";
      }
      if(element.localName === "option") {
          element.parentElement.selectedIndex = 0;
          // console.log("-------->>>>its a option element. element.parentElement.parentElement: ",element.parentElement.parentElement)
      }
  })
  return "cleared form"
}



function elipseMultiLineText(element,max_length) {
  console.log(element.innerText.length)
  if(element.innerText.length > max_length) {
    element.innerText = element.innerText.slice(0,max_length) + "..."
  }
}

//Function was optimized assuming desings rem font values were determined from designs being max_size of each range.
// const ScreenRangesObject = {
//   min_size_mobile: 300,
//   max_size_mobile: 599,
//   min_size_tablet: 600,
//   max_size_tablet:850,
//   min_size_desktop:851,
//   max_size_desktop:1920
// }
//
// const FontResizingObject = {
//   min_mobile_font_size: 5,
//   min_tablet_font_size: 5,
//   min_desktop_font_size: 5
// }
//
// if(window.innerWidth >= ScreenRangesObject.min_size_mobile && window.innerWidth <= ScreenRangesObject.max_size_mobile) {
//   document.getElementById("html").style["font-size"] = Math.max((window.innerWidth / 600 * 15), FontResizingObject.min_mobile_font_size) +"px";
// }
// else if(window.innerWidth >= ScreenRangesObject.min_size_tablet && window.innerWidth <= ScreenRangesObject.max_size_tablet) {
//   document.getElementById("html").style["font-size"] = Math.max((window.innerWidth / 850 * 7), FontResizingObject.min_tablet_font_size) +"px";
// }
// else if(window.innerWidth >= ScreenRangesObject.min_size_desktop && window.innerWidth <= ScreenRangesObject.max_size_desktop) {
//   document.getElementById("html").style["font-size"] = Math.max((window.innerWidth / 1920 * 15), FontResizingObject.min_desktop_font_size) +"px";
//   console.log(Math.max((window.innerWidth / 1920 * 10), FontResizingObject.min_desktop_font_size) +"px")
// }
//
//
// window.addEventListener('resize', function resize() {
//   if(window.innerWidth >= ScreenRangesObject.min_size_mobile && window.innerWidth <= ScreenRangesObject.max_size_mobile) {
//     document.getElementById("html").style["font-size"] = Math.max((window.innerWidth / 600 * 15), FontResizingObject.min_mobile_font_size) +"px";
//   }
//   else if(window.innerWidth >= ScreenRangesObject.min_size_tablet && window.innerWidth <= ScreenRangesObject.max_size_tablet) {
//     document.getElementById("html").style["font-size"] = Math.max((window.innerWidth / 850 * 7), FontResizingObject.min_tablet_font_size) +"px";
//     console.log("resize trigger=",Math.max((window.innerWidth / 850 * 15), FontResizingObject.min_tablet_font_size) +"px")
//   }
//   else if(window.innerWidth >= ScreenRangesObject.min_size_desktop && window.innerWidth <= ScreenRangesObject.max_size_desktop) {
//     document.getElementById("html").style["font-size"] = Math.max((window.innerWidth / 1920 * 15), FontResizingObject.min_desktop_font_size) +"px";
//     console.log("resize trigger=",Math.max((window.innerWidth / 1920 * 15), FontResizingObject.min_desktop_font_size) +"px")
//   }
// });

function blobToFile(theBlob, fileName){
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    console.log("split=",fileName.split("."))
    let length = fileName.split(".").length;
    length = length - 1;
    console.log("legnth =", length)
    let typeArray = fileName.split(".");
    console.log("type=",typeArray[length]);
    theBlob.type = typeArray[length].toString();
    console.log(theBlob)
    return theBlob;
}

function fileToByteArray(file) {
    return new Promise((resolve, reject) => {
        try {
            let reader = new FileReader();
            let fileByteArray = [];
            reader.readAsArrayBuffer(file);
            reader.onloadend = (evt) => {
                if (evt.target.readyState == FileReader.DONE) {
                    let arrayBuffer = evt.target.result,
                        array = new Uint8Array(arrayBuffer);
                    for (byte of array) {
                        fileByteArray.push(byte);
                    }
                }
                resolve(fileByteArray);
            }
        }
        catch (e) {
            reject(e);
        }
    })
}

function ArrayBufferToBinary(buffer) {
    var uint8 = new Uint8Array(buffer);
    return uint8.reduce((binary, uint8) => binary + uint8.toString(2), "");
}

function fileTOArrayBuffer(file) {
    // preliminary code to handle getting local file and finally printing to console
  // the results of our function ArrayBufferToBinary().
  // var file = // get handle to local file.
  var reader = new FileReader();
  reader.onload = function(event) {
      var data = event.target.result;
      console.log(ArrayBufferToBinary(data));
  };
  return reader.readAsArrayBuffer(file); //gets an ArrayBuffer of the file
}

function fileToBinaryString(file) {
 let buffer = fileTOArrayBuffer(file);
 return ArrayBufferToBinary(buffer)
}

async function getByteArray(file) {
    //Get file from your input element
    let myFile = file;

    //Wait for the file to be converted to a byteArray
    let byteArray = fileToByteArray(myFile);
    //Do something with the byteArray
    console.log(byteArray);
    return byteArray;
}
