
// window.addEventListener('load', (event) => {



  document.getElementById("google-login").addEventListener("click",(event)=> {
    console.log("clicked google login")
    loginWithGoogle()
  })



  //
  // function initMap() {
  //   // initialize the global map variable
  //   map = new google.maps.Map(document.getElementById('map'), {
  //     center: {
  //       lat: 0,
  //       lng: 0
  //     },
  //     zoom: 1
  //   });
  //   var watchID = navigator.geolocation.watchPosition(onSuccess, onError, {
  //     timeout: 5000
  //   });
  // }
  //
  // function onSuccess(position) {
  //   var lat = position.coords.latitude;
  //   var lang = position.coords.longitude;
  //   var myLatlng = new google.maps.LatLng(lat, lang);
  //   map.setCenter(myLatlng);
  //   map.setZoom(18);
  //   if (marker && marker.setPosition)
  //     marker.setMap(myLatlng); // move the marker
  //   else // create a marker
  //     marker = new google.maps.Marker({
  //     position: myLatlng,
  //     map: map
  //   });
  // }
  //
  // function onError(error) {
  //   console.log('ERROR(' + error.code + '): ' + error.message);
  // }


window.addEventListener("load", (event)=> {
  // Get geoloation permission and save to global object

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        console.log(pos)
        map.setCenter(pos)
        globalObj.location.lat = pos.lat;
        globalObj.location.lng = pos.lng;
        if(globalObj.location.lat != 0) {
          let myLatLng = { lat: globalObj.location.lat, lng: globalObj.location.lng }
        }
      }
    )
  }
  // add places autocomplete

    // substantiate view switcher
    const centerControlDiv = document.createElement("div");
    CenterControl(centerControlDiv, map);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(centerControlDiv);
    // substantiate map CenterControlFilters
    const centerControlFilter = document.createElement("div");
    const searchControl = document.createElement("div");
    const settingControl = document.createElement("div");
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(settingControl);

    setTimeout(function(){
      CenterControlFilters(centerControlFilter, map);
      addressSearchInput(searchControl,map)
      map.controls[google.maps.ControlPosition.TOP_CENTER].push(searchControl);
      settingsIcon(settingControl, map);

      map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlFilter);

    }, 1000);

  //Use callback funtion to get leads from database and add them to the map
  let location = ""
  setTimeout(function(){
    console.log("about to execute getLeads()")
    getLeads();
    map.addListener("click", (mapsMouseEvent) => {
        // Close the current InfoWindow.
        console.log("clicked map this time")
        document.getElementById("status-filters").hidden = true;
        if (globalObj.currentMarker[0] && globalObj.currentMarker[0].title == "click location") {
          globalObj.currentMarker[0].setMap(null)
        }
        if(globalObj.currentMarker[0]) {
          console.log("just clsoing info window of most recent=",globalObj.currentMarker[0]);
          globalObj.currentInfowindow[0].close(map,globalObj.currentMarker[0]);
        }
        location = mapsMouseEvent.latLng.toJSON();
        console.log(location.lat)
        const marker = new google.maps.Marker({
        position: location,
        map,
        title: "click location",
        icon:svgMarkerNew
        })
        globalObj.currentMarker[0] = marker;
        let pos = `${location.lat},${location.lng}`
        console.log("pos=",pos)
        var GeocodeCoord = (pos) => {
          return Promise.resolve(GeocodeCoordinates(pos));
        };
        GeocodeCoord(pos).then((result)=> {
          console.log(pos)
          console.log("Chained promise. Result of Geocode = ",result.address_components)
           // const myPromise = new Promise((resolve, reject) => {
             const infowindow = new google.maps.InfoWindow({
               content: addLeadInfoWindow(result,pos),
             });
             globalObj.currentInfowindow[0] = infowindow;
             setTimeout(function(){
               leadFormConditionalInputsEL();
               mirrorFormsInputs()
               Array.from(document.getElementsByClassName("save-lead-button")).forEach((element) => {
                 // element.setAttribute("onclick", "leadFormSubmit()");
                 element.addEventListener("click", (event) => {
                   event.preventDefault();
                   console.log("clicked save-lead-button");
                   leadFormSubmit(event);
                 })
               })
             }, 500)

             google.maps.event.addListener(infowindow,'closeclick',function(){
               // currentMark.setMap(null); //removes the marker
               console.log("closed info window")
               let markers = globalObj.currentMarker;
               function setMapOnAll(map) {
                 for (let i = 0; i < markers.length; i++) {
                   markers[i].setMap(map);
                 }
               }
               // Removes the markers from the map, but keeps them in the array.
               function clearMarkers() {
                 setMapOnAll(null);
               }
               // Shows any markers currently in the array.
               function showMarkers() {
                 setMapOnAll(map);
               }
               (function deleteMarkers() {
                 clearMarkers();
                 markers = globalObj.currentMarker;
               })()
             // then, remove the infowindows name from the array
             });
             // function change() {
              return new Promise(function(resolve, reject) {
                // existingLeadInfoWindowFormatting(lead);
                infowindow.open(map, marker);
                if(screen.width < 650) {
                  changeState("sales-form-mobile");
                }
                globalObj.formModal = true;
                document.getElementById("sales-form-mobile").innerHTML = addLeadInfoWindow(result,pos);
                globalObj.currentInfowindow[0] = infowindow;
                // marker.addListener("click", () => {
                //      infowindow.open(map, marker)
                //      globalObj.currentInfowindow[0] = infowindow;
                //  })

                 // setTimeout(resolve, 500)
                })
              // }
              // change()

        }).then( ()=> {
          setTimeout(function(){


            // leadFormConditionalInputsEL()
          }, 2000)
        })
      })
  }, 300)

  })


  window.addEventListener("resize", function(event) {
      resizeFunction()
  })



function addMarkerEventListener(marker,infowindow,lead,formObj) {
  google.maps.event.addListener(marker, 'click', function() {
    if (globalObj.currentMarker[0]) {
      globalObj.currentMarker[0].setMap(null)
    }
     infowindow.open(map,marker);
     existingLeadInfoWindowFormatting(lead);
     if(screen.width < 650) {
       changeState("sales-form-mobile");
     }
     document.getElementById("status-filters").hidden = true;
     globalObj.formModal = true;
     document.getElementById("sales-form-mobile").innerHTML = existingLeadInfoWindow(formObj);
     globalObj.currentInfowindow[0] = infowindow;
     globalObj.currentMarker[0] = marker;
   });
}


function mirrorFormsInputs() {
  // console.log("running mirrorFormsInputs")
  //Put change eventListeners on mobile form and map chnages to desktop
  document["lead-form"][0].street_address.addEventListener("keyup",(event) => {
    document["lead-form"][1].street_address.value = document["lead-form"][0].street_address.value;
  })
  document["lead-form"][0].city.addEventListener("keyup",(event) => {
    document["lead-form"][1].city.value = document["lead-form"][0].city.value;
  })
  document["lead-form"][0].state.addEventListener("keyup",(event) => {
    document["lead-form"][1].state.value = document["lead-form"][0].state.value;
  })
  document["lead-form"][0].zipcode.addEventListener("keyup",(event) => {
    document["lead-form"][1].zipcode.value = document["lead-form"][0].zipcode.value;
  })
  document["lead-form"][0].first_name.addEventListener("keyup",(event) => {
    document["lead-form"][1].first_name.value = document["lead-form"][0].first_name.value;
  })
  document["lead-form"][0].last_name.addEventListener("keyup",(event) => {
    document["lead-form"][1].last_name.value = document["lead-form"][0].last_name.value;
  })
  document["lead-form"][0].phone.addEventListener("keyup",(event) => {
    document["lead-form"][1].phone.value = document["lead-form"][0].phone.value;
  })
  document["lead-form"][0].email.addEventListener("keyup",(event) => {
    document["lead-form"][1].email.value = document["lead-form"][0].email.value;
  })
  document["lead-form"][0].spouse_name.addEventListener("keyup",(event) => {
    document["lead-form"][1].spouse_name.value = document["lead-form"][0].spouse_name.value;
  })
  document["lead-form"][0].power_bill_amount.addEventListener("keyup",(event) => {
    document["lead-form"][1].power_bill_amount.value = document["lead-form"][0].power_bill_amount.value;
  })
  document["lead-form"][0].lead_notes.addEventListener("keyup",(event) => {
    document["lead-form"][1].lead_notes.value = document["lead-form"][0].lead_notes.value;
  })
  document["lead-form"][0].ql_appointment.addEventListener("change",(event) => {
    document["lead-form"][1].ql_appointment.value = document["lead-form"][0].ql_appointment.value;
    console.log("date input changed")
  })
  document["lead-form"][0].circle_back_reminder.addEventListener("change",(event) => {
    document["lead-form"][1].circle_back_reminder.value = document["lead-form"][0].circle_back_reminder.value;
  })
  document["lead-form"][0].spouse[0].addEventListener("change",(event) => {
    if(document["lead-form"][0].spouse.value == "yes"){
      document["lead-form"][1].spouse.value = "yes";
    }
    else {
      document["lead-form"][1].spouse.value = "no";
    }
  })
  document["lead-form"][0].spouse[1].addEventListener("change",(event) => {
    if(document["lead-form"][0].spouse.value == "yes"){
      document["lead-form"][1].spouse.value = "yes";
    }
    else {
      document["lead-form"][1].spouse.value = "no";
    }
  })
  document["lead-form"][0].lead_status.addEventListener("change",(event) => {
    document["lead-form"][1].lead_status.value = document["lead-form"][0].lead_status.value;
  })
  document["lead-form"][0].ql_add_to_calendar.addEventListener("change",(event) => {
    if(document["lead-form"][0].ql_add_to_calendar.checked == true) {
      document["lead-form"][1].ql_add_to_calendar.checked = true
    }
    else {
      document["lead-form"][1].ql_add_to_calendar.checked = false
    }
  })
  document["lead-form"][0].ql_apt_invite.addEventListener("change",(event) => {
    if(document["lead-form"][0].ql_apt_invite.checked) {
      document["lead-form"][1].ql_apt_invite.checked = true;
    }
    else {
      document["lead-form"][1].ql_apt_invite.checked = false;
    }
  })
  document["lead-form"][0].ni_reason.addEventListener("change",(event) => {
    document["lead-form"][1].ni_reason.value = document["lead-form"][0].ni_reason.value;
  })
  document["lead-form"][0].nq_reason.addEventListener("change",(event) => {
    document["lead-form"][1].nq_reason.value = document["lead-form"][0].nq_reason.value;
  })

  // Put the same eventListeners on dekstop for changes to mobile
  document["lead-form"][1].street_address.addEventListener("keyup",(event) => {
    document["lead-form"][0].street_address.value = document["lead-form"][1].street_address.value;
  })
  document["lead-form"][1].city.addEventListener("keyup",(event) => {
    document["lead-form"][0].city.value = document["lead-form"][1].city.value;
  })
  document["lead-form"][1].state.addEventListener("keyup",(event) => {
    document["lead-form"][0].state.value = document["lead-form"][1].state.value;
  })
  document["lead-form"][1].zipcode.addEventListener("keyup",(event) => {
    document["lead-form"][0].zipcode.value = document["lead-form"][1].zipcode.value;
  })
  document["lead-form"][1].first_name.addEventListener("keyup",(event) => {
    document["lead-form"][0].first_name.value = document["lead-form"][1].first_name.value;
  })
  document["lead-form"][1].last_name.addEventListener("keyup",(event) => {
    document["lead-form"][0].last_name.value = document["lead-form"][1].last_name.value;
  })
  document["lead-form"][1].phone.addEventListener("keyup",(event) => {
    document["lead-form"][0].phone.value = document["lead-form"][1].phone.value;
  })
  document["lead-form"][1].email.addEventListener("keyup",(event) => {
    document["lead-form"][0].email.value = document["lead-form"][1].email.value;
  })
  document["lead-form"][1].street_address.addEventListener("keyup",(event) => {
    document["lead-form"][0].street_address.value = document["lead-form"][1].street_address.value;
  })
  document["lead-form"][1].power_bill_amount.addEventListener("keyup",(event) => {
    document["lead-form"][0].power_bill_amount.value = document["lead-form"][1].power_bill_amount.value;
  })
  document["lead-form"][1].lead_notes.addEventListener("keyup",(event) => {
    document["lead-form"][0].lead_notes.value = document["lead-form"][1].lead_notes.value;
  })
  document["lead-form"][1].ql_appointment.addEventListener("change",(event) => {
    document["lead-form"][0].ql_appointment.value = document["lead-form"][1].ql_appointment.value;
  })
  document["lead-form"][1].circle_back_reminder.addEventListener("change",(event) => {
    document["lead-form"][0].circle_back_reminder.value = document["lead-form"][1].circle_back_reminder.value;
  })
  document["lead-form"][1].spouse[0].addEventListener("change",(event) => {
    console.log("triggred spouse change envet")
    if(document["lead-form"][1].spouse.value == "yes"){
      document["lead-form"][0].spouse.value = "yes";
    }
    else {
      document["lead-form"][0].spouse.value = "no";
    }
  })
  document["lead-form"][1].spouse[1].addEventListener("change",(event) => {
    console.log("triggred spouse change envet")
    if(document["lead-form"][1].spouse.value == "yes"){
      document["lead-form"][0].spouse.value = "yes";
    }
    else {
      document["lead-form"][0].spouse.value = "no";
    }
  })
  document["lead-form"][1].lead_status.addEventListener("change",(event) => {
    document["lead-form"][0].lead_status.value = document["lead-form"][1].lead_status.value;
  })
  document["lead-form"][1].ql_add_to_calendar.addEventListener("change",(event) => {
    if(document["lead-form"][1].ql_add_to_calendar.checked == true) {
      document["lead-form"][0].ql_add_to_calendar.checked = true
    }
    else {
      document["lead-form"][0].ql_add_to_calendar.checked = false
    }
  })
  document["lead-form"][1].ql_apt_invite.addEventListener("change",(event) => {
    if(document["lead-form"][1].ql_apt_invite.checked) {
      document["lead-form"][0].ql_apt_invite.checked = true;
    }
    else {
      document["lead-form"][0].ql_apt_invite.checked = false;
    }
  })
  document["lead-form"][1].ni_reason.addEventListener("change",(event) => {
    document["lead-form"][0].ni_reason.value = document["lead-form"][1].ni_reason.value;
  })
  document["lead-form"][1].nq_reason.addEventListener("change",(event) => {
    document["lead-form"][0].nq_reason.value = document["lead-form"][1].nq_reason.value;
  })

}


document.getElementById("map-view").addEventListener("click",(event)=> {
  history.pushState({state:mapPageState()},'Sales Leads - Map View', '/sales-map');
})

document.getElementById("exit-settings").addEventListener("click",(event)=> {
  window.history.back()
})




// google.maps.event.addListener(infowindow,'closeclick',function(){
// })
