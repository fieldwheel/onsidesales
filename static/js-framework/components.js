const addLeadInfoWindow = (result,pos) =>  `
<form name="lead-form" class="" style="display:flex;flex-direction:column;align-content:center;" method="post" cord="${pos}">
  <h2>ADD LEAD</h2>
  <h3>Address</h3>
  <input class="lead-form-input" type="text" name="street_address" value="${result.address_components[0].long_name + ' ' + result.address_components[1].long_name}" placeholder="street address">
  <input class="lead-form-input" type="text" name="city" value="${(result.address_components[2].types.includes("locality")? result.address_components[2].short_name:result.address_components[3].short_name)}" placeholder="city">
  <input class="lead-form-input" type="text" name="state" value="${(result.address_components[4].types.includes("administrative_area_level_1")?result.address_components[4].short_name:result.address_components[5].short_name)}" placeholder="state">
  <input class="lead-form-input" type="text" name="zipcode" value="${(result.address_components[6].types.includes("postal_code")? result.address_components[6].short_name:result.address_components[7].short_name)}" placeholder="zip">
  <h3>Personal Info</h3>
  <input class="lead-form-input" type="text" name="first_name" value="" placeholder="First name">
  <input class="lead-form-input" type="text" name="last_name" value="" placeholder="Last name">
  <input class="lead-form-input" type="phone" name="phone" value="" placeholder="Phone">
  <input class="lead-form-input" type="email" name="email" value="" placeholder="Email">
  <div class="spouseNameBooleen" style="display:flex">
    <label for="">Spouse/Housemate?</label>
    <input type="radio" name="spouse" value="no" checked>
    <label for="no">No</label>
    <input type="radio" name="spouse" value="yes">
    <label for="male">Yes</label>
  </div>
  <input class="lead-form-input spouseName" type="text" name="spouse_name" value="" placeholder="Spouse's Name" hidden>
  <h3>Lead Info</h3>
  <input class="lead-form-input" type="number" name="power_bill_amount" value="" placeholder="Current Power Bill Amount">
  <textarea class="lead-form-input" name="lead_notes" rows="8" cols="80" placeholder="Lead Notes"></textarea>
  <select class="lead-actions" name="lead_status">
    <option>select status</option>
    <option value="Not home">Not home</option>
    <option value="Not interested">Not interested</option>
    <option value="Not qualified">Not qualified</option>
    <option value="Circle back">Circle back</option>
    <option value="Qualified lead">Qualified lead</option>
  </select>
  <div class="ql-actions-map"id="ql-actions-map" style="display:flex;flex-direction:column" hidden>
    <label for="">Set Appointment:</label>
    <input type="datetime-local" name="ql_appointment" value="0000-00-00T00:00">
    <div class="" style="display:flex">
      <input type="checkbox" name="ql_add_to_calendar" value="">
      <label style="margin-top:2px" for="">Add to calendar</label>
    </div>
    <div class="" style="display:flex">
      <input type="checkbox" name="ql_apt_invite" value="">
      <label style="margin-top:2px" for="">Send calendar reminder to prospective customer</label>
    </div>
    <label for="">Power Bill Upload:</label>
    <input name="power_bill_upload" type="file" accept="image/*" capture="camera">
  </div>
  <div class="cb-actions-map"id="cb-actions-map" style="display:flex;flex-direction:column" hidden>
    <label for="">Set Reminder:</label>
    <input type="datetime-local" name="circle_back_reminder" value="0000-00-00T00:00">
  </div>
  <div class="ni-actions-map" id="ni-actions-map" hidden>
    <label for="">Reason:</label>
    <select class="ni_reason" name="ni_reason">
      <option>Select reason</option>
      <option value="No soliciting">No soliciting</option>
      <option value="Didn't Listen">Didn't Listen</option>
      <option value="Not interested">Not interested</option>
      <option value="Already getting solar">Already getting solar</option>
      <option value="Other">Other</option>
    </select>
  </div>
  <div class="nq-actions-map" id="nq-actions-map" hidden>
    <label for="">Reason:</label>
    <select class="nq_reason" name="nq_reason">
      <option>select reason</option>
      <option value="No soliciting">Renting</option>
      <option value="Didn't Listen">Low credit</option>
      <option value="Not interested">Moving</option>
      <option value="Not interested">Other</option>
    </select>
  </div>
  <button class="save-lead-button" href="#"style="cursor:pointer" EL="false">Save</button>
</form>
`;


const existingLeadInfoWindow = (data) =>  `
<form name="lead-form" class="" style="display:flex;flex-direction:column;align-content:center;height:auto" method="post" cord="${data.cordinates}" lead_id="${data.lead_id}">
  <div style="display:flex;">
  <h2>EXISTING LEAD</h2>
  <img id="edit-lead" class="edit-lead" src="/images/edit.png" style="height:20px;width:20px;background-size:contain;object-fit:contain;margin-left:15px;cursor:pointer">
  </div>
  <h3 style="margin-top:30px">Address</h3>
  <input class="lead-form-input" type="text" name="street_address" value="${data.street_address}" placeholder="street address" disabled>
  <input class="lead-form-input" type="text" name="city" value="${data.city}" placeholder="city" disabled>
  <input class="lead-form-input" type="text" name="state" value="${data.state}" placeholder="state" disabled>
  <input class="lead-form-input" type="text" name="zipcode" value="${data.zipcode}" placeholder="zip" disabled>
  <h3>Personal Info</h3>
  <input class="lead-form-input" type="text" name="first_name" value="${data.first_name}" placeholder="First name" disabled>
  <input class="lead-form-input" type="text" name="last_name" value="${data.last_name}" placeholder="Last name" disabled>
  <input class="lead-form-input" type="phone" name="phone" value="${data.phone}" placeholder="Phone" disabled>
  <input class="lead-form-input" type="email" name="email" value="${data.email}" placeholder="Email" disabled>
  <div class="spouseNameBooleen" style="display:flex">
    <label for="">Spouse/Housemate?</label>
    <input type="radio" name="spouse" value="no" checked disabled>
    <label for="no">No</label>
    <input type="radio" name="spouse" value="yes" disabled>
    <labelfor="male">Yes</label>
  </div>
  <input class="lead-form-input spouseName" type="text" name="spouse_name" value="${data.spouse_name}" placeholder="Spouse's Name" hidden disabled>
  <h3>Lead Info</h3>
  <input class="lead-form-input" type="number" name="power_bill_amount" value="" placeholder="Current Power Bill Amount" disabled>
  <textarea class="lead-form-input" name="lead_notes" rows="8" value="${data.lead_notes}" cols="80" placeholder="Lead Notes" disabled></textarea>
  <select class="lead-actions" name="lead_status" value="${data.lead_status}" disabled>
    <option>select status</option>
    <option value="Not home">Not home</option>
    <option value="Not interested">Not interested</option>
    <option value="Not qualified">Not qualified</option>
    <option value="Circle back">Circle back</option>
    <option value="Qualified lead">Qualified lead</option>
  </select>
  <div class="ql-actions-map" id="ql-actions-map" style="display:flex;flex-direction:column" disabled hidden>
    <label for="">Set Appointment:</label>
    <input type="datetime-local" name="ql_appointment" value="${data.ql_appointment}" disabled>
    <div class="" style="display:flex">
      <input type="checkbox" name="ql_add_to_calendar" value="${data.ql_add_to_calendar}"  disabled>
      <label style="margin-top:2px" for="">Add to calendar</label>
    </div>
    <div class="" style="display:flex">
      <input type="checkbox" name="ql_apt_invite" value="${data.ql_apt_invite}" style="margin-bottom:3px" disabled>
      <label for="" style="margin-top:2px">Send calendar reminder to prospective customer</label>
    </div>
    <label for="">Power Bill Upload:</label>
    <input name="power_bill_upload" type="file" accept="image/*" capture="camera" disabled>
  </div>
  <div class="cb-actions-map" id="cb-actions-map" style="display:flex;flex-direction:column" disabled hidden>
    <label for="">Set Reminder:</label>
    <input type="datetime-local" name="circle_back_reminder" value="${data.circle_back_reminder}" disabled>
  </div>
  <div class="ni-actions-map" id="ni-actions-map" hidden>
    <label for="">Reason:</label>
    <select class="ni_reason" name="ni_reason" disabled value="${data.ni_reason}" disabled>
      <option>Select reason</option>
      <option value="No soliciting">No soliciting</option>
      <option value="Didn't Listen">Didn't Listen</option>
      <option value="Not interested">Not interested</option>
      <option value="Already getting solar">Already getting solar</option>
      <option value="Other">Other</option>
    </select>
  </div>
  <div class="nq-actions-map" id="nq-actions-map" hidden>
    <label for="">Reason:</label>
    <select class="nq_reason"name="nq_reason" value="${data.nq_reason}" disabled>
      <option>select reason</option>
      <option value="Renting">Renting</option>
      <option value="Low credit">Low credit</option>
      <option value="Moving">Moving</option>
      <option value="Other">Other</option>
    </select>
  </div>
  <button class="save-lead-button" href="#" style="cursor:pointer" EL="false" hidden>Save</button>
</form>
`;


const svgMarkerNew = {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "white",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};

window.addEventListener('load', (event) => {
  svgMarkerNew.anchor = new google.maps.Point(30, 80);
});

const svgMarkerNI = {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "black",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerNI.anchor = new google.maps.Point(30, 80);
});

const svgMarkerNH = {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "blue",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerNH.anchor = new google.maps.Point(30, 80);
});

const svgMarkerTeam= {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "gray",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerTeam.anchor = new google.maps.Point(30, 80);
});

const svgMarkerCB= {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "orange",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerCB.anchor = new google.maps.Point(30, 80);
});

const svgMarkerQL= {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "green",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerQL.anchor = new google.maps.Point(30, 80);
});

const svgMarkerNQ= {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "red",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerNQ.anchor = new google.maps.Point(30, 80);
});


const svgMarkerSelect= {
   path:
     "M28.002 0.283997C12.828 0.283997 0.525024 12.583 0.525024 27.759C0.525024 42.931 28.002 81.716 28.002 81.716C28.002 81.716 55.475 42.931 55.475 27.759C55.475 12.583 43.176 0.283997 28.002 0.283997ZM28.002 40.401C21.016 40.401 15.357 34.745 15.357 27.758C15.357 20.771 21.015 15.115 28.002 15.115C34.984 15.115 40.645 20.771 40.645 27.758C40.645 34.745 34.984 40.401 28.002 40.401Z",
   fillColor: "white",
   fillOpacity: 0.8,
   strokeWeight: 0,
   rotation: 0,
   scale: .5,
};
window.addEventListener('load', (event) => {
  svgMarkerSelect.anchor = new google.maps.Point(30, 80);
});

function CenterControl(controlDiv, map) {
  // Set CSS for the control border.
  const controlUI = document.createElement("div");
  controlUI.style.backgroundColor = "#fff";
  controlUI.style.border = "2px solid #fff";
  controlUI.style.borderRadius = "3px";
  controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
  controlUI.style.cursor = "pointer";
  controlUI.style.marginTop = "10px";
  controlUI.style.marginBottom = "22px";
  controlUI.style.marginRight = "10px";
  controlUI.style.marginLeft = "20px";


  controlUI.style.textAlign = "center";
  controlUI.title = "Click to recenter the map";
  controlDiv.appendChild(controlUI);
  // Set CSS for the control interior.
  const controlText = document.createElement("div");
  controlText.style.color = "rgb(25,25,25)";
  controlText.style.fontFamily = "Roboto,Arial,sans-serif";
  controlText.style.fontSize = "16px";
  controlText.style.lineHeight = "38px";
  controlText.style.paddingLeft = "5px";
  controlText.style.paddingRight = "5px";

  controlText.innerHTML = "List View";
  controlUI.appendChild(controlText);
  // Setup the click event listeners: simply set the map to Chicago.
  controlUI.addEventListener("click", () => {
    console.log("clicked List View")
    history.pushState({state:listViewPageState()},'Leads - List View', '/leads-list');
  });
}

function CenterControlFilters(controlDiv, map) {
  // Set CSS for the control border.
  const filterSelect = document.createElement("div");
  filterSelect.style.cssText = "width:150px; border:2px solid #fff; border-radius:3px; background-color:#fff; box-shodow:0 2px 6px rgba(0,0,0,.3);margin-top:7px;cursor:pointer;font-size:16px;display:flex;flex-direction:column";
  const selectLabelElement = document.createElement("div");
  selectLabelElement.id = "status-filters-button";
  selectLabelElement.style.cssText = "display:flex;justify-content:space-between;align-items:center;height:38px;padding-left:10px";
  pTextElement = document.createElement("p");
  pTextElement.style.cssText = "margin:0px;font-size:15px;font-family:Roboto,Arial,sans-serif;color:rgb(25,25,25);line-height: 38px"
  pTextElement.innerText = "Status Filters";
  iArrowElement = document.createElement("p");
  iArrowElement.style.cssText = "margin-right:7px;margin-top:12px;width:0px;height:0px;border: solid black;border-width: 0 3px 3px 0;display: inline-block;padding: 3px;transform: rotate(45deg);-webkit-transform: rotate(45deg);"
  selectLabelElement.appendChild(pTextElement);
  selectLabelElement.appendChild(iArrowElement);
  filterSelect.appendChild(selectLabelElement);
  const filters = document.createElement("div");
  filters.hidden = true;
  filters.id = "status-filters";
  filters.style.cssText = "width:100%;border:2px solid #fff;border-radius:3px;position:relative;left:-2px";

  const filter1 = document.createElement("div");
  filter1.style.cssText = "background-color:#ffffff;";
  const filter1Input = document.createElement("input");
  filter1Input.type = "checkbox";
  filter1Input.style.cssText = "text-align:center;cursor:pointer;"
  filter1Input.id = "circle_back_filter";
  filter1Input.value = "circle_back";
  filter1Input.checked = true;
  const filter1Label = document.createElement("label");
  filter1Label.style.cssText = "position:relative;top:-1px";
  filter1Label.innerText = "Circle Back";
  filter1.appendChild(filter1Input);
  filter1.appendChild(filter1Label);
  filters.appendChild(filter1);

  const filter2 = document.createElement("div");
  filter2.style.cssText = "background-color:#ffffff;";
  const filter2Input = document.createElement("input");
  filter2Input.type = "checkbox";
  filter2Input.style.cssText = "text-align:center;cursor:pointer;"
  filter2Input.id = "qualified_lead_filter";
  filter2Input.value = "qualified_lead";
  filter2Input.checked = true;
  const filter2Label = document.createElement("label");
  filter2Label.style.cssText = "position:relative;top:-1px";
  filter2Label.innerText = "Qualified Lead";
  filter2.appendChild(filter2Input);
  filter2.appendChild(filter2Label);
  filters.appendChild(filter2);

  const filter3 = document.createElement("div");
  filter3.style.cssText = "background-color:#ffffff;";
  const filter3Input = document.createElement("input");
  filter3Input.type = "checkbox";
  filter3Input.style.cssText = "text-align:center;cursor:pointer;"
  filter3Input.id = "not_home_filter";
  filter3Input.value = "not_home";
  filter3Input.checked = true;
  const filter3Label = document.createElement("label");
  filter3Label.style.cssText = "position:relative;top:-1px";
  filter3Label.innerText = "Not Home";
  filter3.appendChild(filter3Input);
  filter3.appendChild(filter3Label);
  filters.appendChild(filter3);

  const filter4 = document.createElement("div");
  filter4.style.cssText = "background-color:#ffffff;";
  const filter4Input = document.createElement("input");
  filter4Input.type = "checkbox";
  filter4Input.style.cssText = "text-align:center;cursor:pointer;"
  filter4Input.id = "not_interested_filter";
  filter4Input.value = "not_interested";
  filter4Input.checked = true;
  const filter4Label = document.createElement("label");
  filter4Label.style.cssText = "position:relative;top:-1px";
  filter4Label.innerText = "Not Interested";
  filter4.appendChild(filter4Input);
  filter4.appendChild(filter4Label);
  filters.appendChild(filter4);

  const filter5 = document.createElement("div");
  filter5.style.cssText = "background-color:#ffffff;";
  const filter5Input = document.createElement("input");
  filter5Input.type = "checkbox";
  filter5Input.style.cssText = "text-align:center;cursor:pointer;"
  filter5Input.id = "not_qualified_filter";
  filter5Input.value = "not_qualified";
  filter5Input.checked = true;
  const filter5Label = document.createElement("label");
  filter5Label.style.cssText = "position:relative;top:-1px";
  filter5Label.innerText = "Not Qualified";
  filter5.appendChild(filter5Input);
  filter5.appendChild(filter5Label);
  filters.appendChild(filter5);

  filterSelect.appendChild(filters);







  const centerControlFilterSelect =
  `<div style="width:150px;border:2px solid #fff;border-radius:3px;background-color:#fff;box-shodow:0 2px 6px rgba(0,0,0,.3);margin-top:8px;cursor:pointer;font-size:16px;display:flex;flex-direction:column">
      <div id="status-filters-button" style="display:flex;justify-content:space-between;align-items:center;height:39px;padding-left:10px">
        <p style="margin:0px;font-size:15px;font-family:Roboto,Arial,sans-serif;color:rgb(25,25,25);line-height: 38px">Status Filters</p>
        <i style="margin-right:7px;margin-top:-2px;width:0px;height:0px;border: solid black;border-width: 0 3px 3px 0;display: inline-block;padding: 3px;transform: rotate(45deg);-webkit-transform: rotate(45deg);"></i>
      </div>

      <div id="status-filters" style="width:100%;border:2px solid #fff;border-radius:3px;position:relative;left:-2px; user-select: none;"hidden>
        <div style="background-color:#ffffff;">
          <input style="text-align:center;cursor:pointer;" type="checkbox" id="circle_back_filter" value="circle_back" checked></input>
          <label style="position:relative;top:-1px">Circle Back</label>
        </div>
        <div style="background-color:#ffffff">
          <input style="text-align:center;cursor:pointer" type="checkbox" id="qualified_lead_filter" value="qualified_lead" checked></input>
          <label style="position:relative;top:-1px">Qualified Lead</label>
        </div>
        <div style="background-color:#ffffff">
          <input style="text-align:center;cursor:pointer" type="checkbox" id="not_home_filter" value="not_home" checked></input>
          <label style="position:relative;top:-1px">Not Home</label>
        </div>
        <div style="background-color:#ffffff">
          <input style="text-align:center:cursor:pointer" type="checkbox" id="not_interested_filter" value="not_interested" checked></input>
          <label style="position:relative;top:-1px">Not Interested</label>
        </div>
        <div style="background-color:#ffffff">
          <input style="text-align:center;cursor:pointer" type="checkbox" id="not_qualified_filter" value="not_qualified" checked></input>
          <label style="position:relative;top:-1px">Not Qualified</label>
        </div>
      </div>

    </div>`
  controlDiv.appendChild(filterSelect);
  selectLabelElement.addEventListener("click", () => {
    console.log("clicked status filter");
    let filters = document.getElementById("status-filters");
    if(filters.hidden == false) {
      filters.hidden = true;
    }
    else {
      filters.hidden = false;
    }
  });
  filters.addEventListener("click",(event)=> {
    let filtersArray = [];
    if(event.target.value == "circle_back" || event.target.value == "qualified_lead" || event.target.value == "not_home" || event.target.value == "not_interested" || event.target.value == "not_qualified") {
      if(document.getElementById("circle_back_filter").checked) {
        filtersArray.push("Circle back")
      }
      if(document.getElementById("qualified_lead_filter").checked) {
        filtersArray.push("Qualified lead")
      }
      if(document.getElementById("not_home_filter").checked) {
        filtersArray.push("Not home")
      }
      if(document.getElementById("not_interested_filter").checked) {
        filtersArray.push("Not interested")
      }
      if(document.getElementById("not_qualified_filter").checked) {
        filtersArray.push("Not qualified")
      }
    }
    // Delete all the markers currently on the map
    for (let i = 0; i < globalObj.markers.length; i++) {
       globalObj.markers[i].setMap(null);
     }
     // Reset markers that are in the filterArray
     globalObj.markers.forEach((marker)=> {
       if(filtersArray.includes(marker.lead_status)) {
         marker.setMap(map);
       }
     })
  })
  // Setup the click event listeners: simply set the map to Chicago.
}

function addressSearchInput(controlDiv,map) {
  // const searchInput = "<input id='search-input' placeholder='Search locate address'style='text-indent:3px;height:40px;width:200px;margin-top:7px;border:1px;border-radius:4px 0px 0px 4px;font-size:15px;border-right:1px solid black;outline:none' type='text'></input><button style='font-size:15px;position:relative;top:1px;height:44px;width:70px;margin-right:10px;background-color:white;margin-left:-2px;box-shabow:0px;border-color:white;border-radius:0px 3px 3px 0px;padding-right:3px;cursor:pointer' id='address-search'>Search</buttom>";
  const search = document.createElement("input");
  search.id = 'search-input';
  search.placeholder = "Search locate address";
  search.style.cssText = "text-indent:3px;height:40px;width:200px;margin-top:7px;border-style:none;border:1px;border-radius:4px 0px 0px 4px;font-size:15px;border-right:1px solid black;outline:none"
  search.type = "text";
  const searchButton = document.createElement("button");
  searchButton.innerText = "Search";
  searchButton.id = "address-search";
  searchButton.style.cssText = "font-size:15px;position:relative;height:42px;width:70px;margin-right:10px;background-color:white;margin-left:-2px;box-shabow:0px;border-color:white;border-radius:0px 3px 3px 0px;padding-right:3px;cursor:pointer";
  controlDiv.appendChild(search);
  controlDiv.appendChild(searchButton);
  searchButton.addEventListener("click",(event)=> {
    console.log("clicked search");
    let address = document.getElementById("search-input").value;
    if(address.length == 0) {
      return false
    }
    else {
      reverseGeocodeCoordinates(address);
    }
  });
}

function settingsIcon(controlDiv,map) {
  // const searchInput = "<img id='settings-icon' style='height:30px;width:30px;background-color:white;margin-top:8px;margin-right:12px;padding:10px;cursor:pointer' src='/images/settings-icon.png'/>";
  const settingsIcon = document.createElement("img");
  settingsIcon.img = "settings-icon";
  settingsIcon.style.cssText = "height:30px;width:30px;background-color:white;margin-top:8px;margin-right:12px;padding:10px;cursor:pointer"
  settingsIcon.src = '/images/settings-icon.png';
  controlDiv.appendChild(settingsIcon);
  settingsIcon.addEventListener("click",(event)=> {
    history.pushState({state:settingsPageState()},'Settings', '/settings');
  })
}





// <path xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" sodipodi:nodetypes="sscccccsscs" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" inkscape:connector-curvature="0" id="path4337-3" d="m 817.11249,282.97118 c -1.25816,1.34277 -2.04623,3.29881 -2.01563,5.13867 0.0639,3.84476 1.79693,5.3002 4.56836,10.59179 0.99832,2.32851 2.04027,4.79237 3.03125,8.87305 0.13772,0.60193 0.27203,1.16104 0.33416,1.20948 0.0621,0.0485 0.19644,-0.51262 0.33416,-1.11455 0.99098,-4.08068 2.03293,-6.54258 3.03125,-8.87109 2.77143,-5.29159 4.50444,-6.74704 4.56836,-10.5918 0.0306,-1.83986 -0.75942,-3.79785 -2.01758,-5.14062 -1.43724,-1.53389 -3.60504,-2.66908 -5.91619,-2.71655 -2.31115,-0.0475 -4.4809,1.08773 -5.91814,2.62162 z" style="display:inline;opacity:1;fill:#ff4646;fill-opacity:1;stroke:#d73534;stroke-width:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"/>

// });
