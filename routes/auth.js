'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */


// const http = require('http');
const url = require('url');
const querystring = require('querystring');
// const opn = require('opn');

//Setting up knex
const env = process.env.NODE_ENV ||'development';
const config = require('../knexfile')[env];
const knex = require('knex')(config);

const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');

const axios = require('axios');

router.get("/:token", function (req, res) {
  console.log('checking to see if user is already logged-in');
  console.log(req.params.token);
  if(req.params.token) {
      knex('sessions').where('unique_token', req.params.token).first()
      .then((session)=> {
          console.log('session object', session);
          // console.log(session.session_start_time)
          console.log(Date.now()+604800000)
          if(session) {
              if(session.session_start_time < Date.now()+604800000 ){
                  console.log('user is logged in');
                  res.status(200).send({message: 'logged in'});
              }
          }
          else {
              console.log('user is not logged in')
              res.status(200).send({message: 'not logged in'})
          }
      })
      .catch((error)=> {
          res.status(500).send(error);
          console.log(error);
      })
  }
  else {
      res.status(200).send({message: 'not logged in'})
  }
})


module.exports = router;
