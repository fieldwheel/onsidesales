'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */

const {OAuth2Client} = require('google-auth-library');
// const http = require('http');
const url = require('url');
const querystring = require('querystring');
// const opn = require('opn');

//Setting up knex
const env = process.env.NODE_ENV ||'development';
const config = require('../knexfile')[env];
const knex = require('knex')(config);

const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');

const axios = require('axios');
const fetch = require('node-fetch')
// const clientID = 88397140807-no0hqvvanqhs24pga932teicq1bnq77s.apps.googleusercontent.com;
// const ClientSecret = AjS3C7xcOJi-Hzd0H3HyFCzI;

const keys = require('../client_secret_88397140807-no0hqvvanqhs24pga932teicq1bnq77s.apps.googleusercontent.com.json');
const authcookie = '2468'


const oAuth2Client = new OAuth2Client(
    keys.web.client_id,
    keys.web.client_secret,
    keys.web.redirect_uris[0]
  );


// http://localhost:8000/user/google-auth-login?state=2468&code=4/0AY0e-g6B3y0NBGRSmD0XnoS6uVRlXuVOytE7WxsJSn6iKwxWEFPsrAqaoS27a_fm1wPqbw&scope=email%20profile%20https://www.googleapis.com/auth/spreadsheets%20https://www.googleapis.com/auth/userinfo.profile%20https://www.googleapis.com/auth/userinfo.email%20openid&authuser=0&hd=fieldwheel.com&prompt=consent
// {"response":
// {"config":
// {"method":"POST","url":"https://oauth2.googleapis.com/token","data":"code=4%2F0AY0e-g6B3y0NBGRSmD0XnoS6uVRlXuVOytE7WxsJSn6iKwxWEFPsrAqaoS27a_fm1wPqbw&client_id=88397140807-no0hqvvanqhs24pga932teicq1bnq77s.apps.googleusercontent.com&client_secret=AjS3C7xcOJi-Hzd0H3HyFCzI&redirect_uri=https%3A%2F%2Fwww.onsidesales.com%2Fsales-map&grant_type=authorization_code&code_verifier=","headers":{"Content-Type":"application/x-www-form-urlencoded","User-Agent":"google-api-nodejs-client/7.0.4","x-goog-api-client":"gl-node/15.12.0 auth/7.0.4","Accept":"application/json"},"body":"code=4%2F0AY0e-g6B3y0NBGRSmD0XnoS6uVRlXuVOytE7WxsJSn6iKwxWEFPsrAqaoS27a_fm1wPqbw&client_id=88397140807-no0hqvvanqhs24pga932teicq1bnq77s.apps.googleusercontent.com&client_secret=AjS3C7xcOJi-Hzd0H3HyFCzI&redirect_uri=https%3A%2F%2Fwww.onsidesales.com%2Fsales-map&grant_type=authorization_code&code_verifier=","responseType":"json"},"data":{"error":"redirect_uri_mismatch","error_description":"Bad Request"},"headers":{"alt-svc":"h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"","cache-control":"no-cache, no-store, max-age=0, must-revalidate","connection":"close","content-encoding":"gzip","content-type":"application/json; charset=utf-8","date":"Tue, 13 Apr 2021 16:09:39 GMT","expires":"Mon, 01 Jan 1990 00:00:00 GMT","pragma":"no-cache","server":"scaffolding on HTTPServer2","transfer-encoding":"chunked","vary":"Origin, X-Origin, Referer","x-content-type-options":"nosniff","x-frame-options":"SAMEORIGIN","x-xss-protection":"0"},"status":400,"statusText":"Bad Request","request":{"responseURL":"https://oauth2.googleapis.com/token"}},"config":{"method":"POST","url":"https://oauth2.googleapis.com/token","data":"code=4%2F0AY0e-g6B3y0NBGRSmD0XnoS6uVRlXuVOytE7WxsJSn6iKwxWEFPsrAqaoS27a_fm1wPqbw&client_id=88397140807-no0hqvvanqhs24pga932teicq1bnq77s.apps.googleusercontent.com&client_secret=AjS3C7xcOJi-Hzd0H3HyFCzI&redirect_uri=https%3A%2F%2Fwww.onsidesales.com%2Fsales-map&grant_type=authorization_code&code_verifier=","headers":{"Content-Type":"application/x-www-form-urlencoded","User-Agent":"google-api-nodejs-client/7.0.4","x-goog-api-client":"gl-node/15.12.0 auth/7.0.4","Accept":"application/json"},"body":"code=4%2F0AY0e-g6B3y0NBGRSmD0XnoS6uVRlXuVOytE7WxsJSn6iKwxWEFPsrAqaoS27a_fm1wPqbw&client_id=88397140807-no0hqvvanqhs24pga932teicq1bnq77s.apps.googleusercontent.com&client_secret=AjS3C7xcOJi-Hzd0H3HyFCzI&redirect_uri=https%3A%2F%2Fwww.onsidesales.com%2Fsales-map&grant_type=authorization_code&code_verifier=","responseType":"json"},"code":"400"}


router.get("/google-auth-login", function (req, res) {
    console.log('google-auth route hit');
    // console.log("Request 2=",req)
    if (req.query.code) {
        console.log("req.query=",req.query)
        oAuth2Client.getToken(req.query.code)
            .then((tokens)=> {
                console.log('tokens: ',tokens);
                console.log("access_token:", tokens.res.data.access_token);
                console.log("token_type:", tokens.res.data.token_type);
                console.log("scope:", tokens.res.data.scope);
                console.log("expiry_date:", tokens.res.data.expiry_date);
                if(tokens.res.data.refresh_token) {
                    console.log("refresh_token:", tokens.res.data.refresh_token);
                }
                else {
                    console.log("no refresh_token")
                }
                // oAuth2Client.setCredentials(tokens);
                console.log("fetching user profile")
                fetch(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${tokens.res.data.access_token}`)
                    .then((user_profile_response) => {
                        console.log("-------->>>>Retreived google profile data response: ", user_profile_response)
                        return user_profile_response.json()
                            .then((user_data)=> {
                                console.log("-------->>>> Successfully retreived google profile data for this user. user_data: ", user_data)
                                console.log(`-------->>>> Checking to see if that user's email (${user_data.email}) exists in our system`)
                                return knex('users').where('email', user_data.email).first()
                                .then((user)=> {
                                    //if the user has created an account before, then see if they have linked their google account
                                    if(user && user.length !== 0) {

                                        console.log("-------->>>> The user's email already exists. Checking to see if user_id in the google_auth table...")

                                        return knex('google_auth').where('user_id', user.user_id).first()
                                            .then((user_google_auth)=> {
                                                if(user_google_auth && user_google_auth.length !== 0) {
                                                    console.log("-------->>>> User's google_auth record exists. IF they have ever revoked access the refresh token will be supplied again. Looking in the response object for a refresh_token fom Google...")
                                                    if(tokens.res.data.refresh_token) {
                                                        console.log("-------->>>> A refresh token was provided, update the google_auth record with the new refresh_token")
                                                        return knex('google_auth').where('user_id', user.user_id).update( {
                                                            refresh_token: tokens.res.data.refresh_token,
                                                            access_token: tokens.res.data.access_token,
                                                            token_type: tokens.res.data.token_type,
                                                            scope: 'https://www.googleapis.com/auth/drive.file',
                                                            expiry_date: tokens.res.data.expiry_date,
                                                        })
                                                            .then(()=> {
                                                                console.log('-------->>>> Checking database for a prior session from this user...')
                                                                console.log("user.user_id=",user.user_id)
                                                                return knex('sessions').where('user_id', user.user_id)
                                                                .then( (session) => {
                                                                    if(session && session.length !== 0) {
                                                                        console.log('-------->>>> User had a previous session: ', session);
                                                                        let token = uuidv4();
                                                                        console.log("-------->>>> Updating session...")
                                                                        // console.log(user_graph);
                                                                        return knex('sessions')
                                                                            .where('user_id', user.user_id)
                                                                            .update( {
                                                                            unique_token: token
                                                                            })
                                                                            .then( () => {
                                                                                console.log('-------->>>> Sending response, saving cookie to client, and redrircting user to their Dashboard...', token)
                                                                                res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                                                    // oAuth2Client.on('tokens', (tokens) => {
                                                                                    //     if (tokens.refresh_token) {
                                                                                    //       // store the refresh_token in my database // at a specified user_id!
                                                                                    //       console.log(tokens.refresh_token);
                                                                                    //     }
                                                                                    //     console.log(tokens.access_token);
                                                                                    //   });

                                                                            })
                                                                            .catch( (error)=> {
                                                                                console.log('error setting cookie')
                                                                                res.status(500).send(error);
                                                                            })
                                                                        }
                                                                        else {
                                                                            console.log('-------->>>> User has not had a session before. Inserting session token...');
                                                                            let token = uuidv4();
                                                                            console.log("token=",token)
                                                                            return knex('sessions').insert( {
                                                                                user_id: user.user_id,
                                                                                unique_token: token
                                                                            })
                                                                            .then(() => {
                                                                                console.log(`-------->>>> Sending response, saving cookie token(${token}) to client , and redrircting user to their Dashboard...`)
                                                                                // let returnObj = {message: "success", token: token};
                                                                                res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                                                // oAuth2Client.on('tokens', (tokens) => {
                                                                                //     if (tokens.refresh_token) {
                                                                                //       // store the refresh_token in my database // at a specified user_id!
                                                                                //       console.log(tokens.refresh_token);
                                                                                //     }
                                                                                //     console.log(tokens.access_token);
                                                                                //   });
                                                                            })
                                                                            .catch( (error)=> {
                                                                                res.status(500).send(error);
                                                                            })
                                                                        }
                                                                })
                                                                .catch( (error)=> {
                                                                    res.status(500).send(error);
                                                                })
                                                            })
                                                            .catch( (error)=> {
                                                                res.status(500).send(error);
                                                            })

                                                    }
                                                    else {
                                                        console.log("-------->>>> A refreh token was not provided, so just update the access_token and expiry_date")
                                                        return knex('google_auth').where('user_id', user.user_id).update( {
                                                            access_token: tokens.res.data.access_token,
                                                            expiry_date: tokens.res.data.expiry_date,
                                                        })
                                                            .then( ()=> {
                                                                console.log('-------->>>> Checking database for a prior session from this user...')
                                                                return knex('sessions').where('user_id', user.user_id)
                                                                .then( (session) => {
                                                                    if(session && session.length !== 0) {
                                                                        console.log('-------->>>> User had a previous session: ', session);
                                                                        let token = uuidv4();
                                                                        console.log("-------->>>> Updating session...")
                                                                        // console.log(user_graph);
                                                                        return knex('sessions')
                                                                            .where('user_id', user.user_id)
                                                                            .update( {
                                                                            unique_token: token
                                                                            })
                                                                            .then( () => {
                                                                                console.log('-------->>>> Sending response, saving cookie to client, and redrircting user to their Dashboard...', token)
                                                                                res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                                                    // oAuth2Client.on('tokens', (tokens) => {
                                                                                    //     if (tokens.refresh_token) {
                                                                                    //       // store the refresh_token in my database // at a specified user_id!
                                                                                    //       console.log(tokens.refresh_token);
                                                                                    //     }
                                                                                    //     console.log(tokens.access_token);
                                                                                    //   });

                                                                            })
                                                                            .catch( (error)=> {
                                                                                console.log('error setting cookie')
                                                                                res.status(500).send(error);
                                                                            })
                                                                        }
                                                                        else {
                                                                            console.log('-------->>>> User has not had a session before. Inserting session token...');
                                                                            let token = uuidv4();
                                                                            return knex('sessions').insert( {
                                                                                user_id: user.user_id,
                                                                                unique_token: token
                                                                            })
                                                                            .then(() => {
                                                                                console.log(`-------->>>> Sending response, saving cookie token(${token}) to client, and redrircting user to their Dashboard...`)
                                                                                // let returnObj = {message: "success", token: token};
                                                                                res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                                                // oAuth2Client.on('tokens', (tokens) => {
                                                                                //     if (tokens.refresh_token) {
                                                                                //       // store the refresh_token in my database // at a specified user_id!
                                                                                //       console.log(tokens.refresh_token);
                                                                                //     }
                                                                                //     console.log(tokens.access_token);
                                                                                //   });
                                                                            })
                                                                            .catch( (error)=> {
                                                                                res.status(500).send(error);
                                                                            })
                                                                        }
                                                                })
                                                                .catch( (error)=> {
                                                                    res.status(500).send(error);
                                                                })
                                                            })
                                                            .catch( (error)=> {
                                                                res.status(500).send(error);
                                                            })

                                                    }
                                                }
                                                else {
                                                    console.log("This user has never enabled google_auth. Insert a full google_auth record for this user")
                                                    return knex('google_auth').insert( {
                                                        refresh_token: tokens.res.data.refresh_token,
                                                        access_token: tokens.res.data.access_token,
                                                        token_type: tokens.res.data.token_type,
                                                        scope: 'https://www.googleapis.com/auth/drive.file',
                                                        expiry_date: tokens.res.data.expiry_date,
                                                    })
                                                        .then(()=> {
                                                            console.log('-------->>>> Checking database for a prior session from this user...')
                                                            return knex('sessions').where('user_id', user_graph.user_id)
                                                            .then( (session) => {
                                                                if(session && session.length !== 0) {
                                                                    console.log('-------->>>> User had a previous session: ', session);
                                                                    let token = uuidv4();
                                                                    console.log("-------->>>> Updating session...")
                                                                    // console.log(user_graph);
                                                                    return knex('sessions')
                                                                        .where('user_id', user.user_id)
                                                                        .update( {
                                                                        unique_token: token
                                                                        })
                                                                        .then( () => {
                                                                            console.log('-------->>>> Sending response, saving cookie to client, and redrircting user to their Dashboard...', token)
                                                                            res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                                                // oAuth2Client.on('tokens', (tokens) => {
                                                                                //     if (tokens.refresh_token) {
                                                                                //       // store the refresh_token in my database // at a specified user_id!
                                                                                //       console.log(tokens.refresh_token);
                                                                                //     }
                                                                                //     console.log(tokens.access_token);
                                                                                //   });

                                                                        })
                                                                        .catch( (error)=> {
                                                                            console.log('error setting cookie')
                                                                            res.status(500).send(error);
                                                                        })
                                                                    }
                                                                    else {
                                                                        console.log('-------->>>> User has not had a session before. Inserting session token...');
                                                                        let token = uuidv4();
                                                                        return knex('sessions').insert( {
                                                                            user_id: user.user_id,
                                                                            unique_token: token
                                                                        })
                                                                        .then(() => {
                                                                            console.log(`-------->>>> Sending response, saving cookie token(${token}) to client, and redrircting user to their Dashboard...`)
                                                                            // let returnObj = {message: "success", token: token};
                                                                            res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                                            // oAuth2Client.on('tokens', (tokens) => {
                                                                            //     if (tokens.refresh_token) {
                                                                            //       // store the refresh_token in my database // at a specified user_id!
                                                                            //       console.log(tokens.refresh_token);
                                                                            //     }
                                                                            //     console.log(tokens.access_token);
                                                                            //   });
                                                                        })
                                                                        .catch( (error)=> {
                                                                            res.status(500).send(error);
                                                                        })
                                                                    }
                                                            })
                                                            .catch( (error)=> {
                                                                res.status(500).send(error);
                                                            })
                                                        })
                                                        .catch( (error)=> {
                                                            res.status(500).send(error);
                                                        })

                                                }
                                            })
                                            .catch( (error)=> {
                                                res.status(500).send(error);
                                            })


                                    }
                                    else {
                                        console.log("-------->>>> This is a first time user. Adding email to database and returning a unique user_id...")
                                        return knex('users').returning('user_id').insert({
                                            email: user_data.email
                                        })
                                        .then((user_id)=> {
                                            console.log(`-------->>>> Adding first time user (${user_id[0]}) to google_auth records...`)
                                            return knex('google_auth').insert({
                                                user_id: user_id[0],
                                                refresh_token: tokens.res.data.refresh_token,
                                                access_token: tokens.res.data.access_token,
                                                token_type: tokens.res.data.token_type,
                                                scope: '[https://www.googleapis.com/auth/drive.file,https://www.googleapis.com/auth/userinfo.profile,https://www.googleapis.com/auth/userinfo.email]',
                                                expiry_date: tokens.res.data.expiry_date,
                                            })
                                            .then(()=> {
                                                console.log('-------->>>>Generating and inserting session_token to database...');
                                                let token = uuidv4();
                                                console.log("token=",token)
                                                return knex('sessions').insert( {
                                                    user_id: user_id[0],
                                                    unique_token: token
                                                })
                                                .then(() => {
                                                    console.log(`-------->>>> Sending response, saving cookie token(${token}) to client, and redrircting user to their Dashboard...`)
                                                    res.cookie('token', token, {httpOnly: true}).status(200).redirect('/sales-map');
                                                    // oAuth2Client.on('tokens', (tokens) => {
                                                    //     if (tokens.refresh_token) {
                                                    //       // store the refresh_token in my database // at a specified user_id!
                                                    //       console.log(tokens.refresh_token);
                                                    //     }
                                                    //     console.log(tokens.access_token);
                                                    //   });
                                                })
                                                .catch( (error)=> {
                                                    res.status(500).send(error);
                                                })
                                            })
                                            .catch((error)=> {
                                                res.status(500).send(error);
                                            })
                                        })
                                        .catch((error)=> {
                                            res.status(500).send(error);
                                        })
                                    }
                                })
                                .catch( (error)=> {
                                    res.status(500).send(error);
                                })
                            })
                            .catch( (error)=> {
                                res.status(500).send(error);
                            })
                    })
                    .catch( (error)=> {
                        res.status(500).send(error);
                    })
                })
                .catch( (error)=> {
                    res.status(500).send(error);
                })
            }
        else {
            console.log("succesfully navigated to google redirect");
            const scopes =  ['https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/calendar.events' ];
            const url = oAuth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: scopes,
                redirect_uri: 'http://localhost:8000/user/google-auth-login',
                state: authcookie,
                include_granted_scopes: true
            })
            res.status(200).send({message: 'success', url_redirect: url})
        }

    // GOOGLE SHEETS INTEGRATION STARTS HERE



// Download your OAuth2 configuration from the Google


// function firstTimeAuth () {
//     fetch(`https://accounts.google.com/o/oauth2/v2/auth?client_id=962756333728-jn3fnn6o53ndfcsnjrai55m53ibndk2d.apps.googleusercontent.com&redirect_uri=https://www.intelifins.com/dashboard&scope=https://www.googleapis.com/auth/drive.file&access_type=offline&state=${authcookie}&include_granted_scopes=true&prompt=consent`)
//     .then((res) => {
//         console.log('response:',res)
//         if(res.json()) {
//             res.json()
//         }
//         else {
//             return res
//         }
//     })
//     .then(json => {
//         console.log(json)
//         res.status(200).send({message: "success", json: json})
//     })
//     .catch(error=> console.log(error))
// }

// firstTimeAuth()







/**
    //  * Start by acquiring a pre-authenticated oAuth2 client.
    //  */
    // async function main() {
    //   try {
    //     const oAuth2Client = await getAuthenticatedClient();
    //     // Make a simple request to the Google Plus API using our pre-authenticated client. The `request()` method
    //     // takes an AxiosRequestConfig object.  Visit https://github.com/axios/axios#request-config.
    //   } catch (e) {
    //     console.error(e);
    //   }
    //   process.exit();
    // }

    /**
     * Create a new OAuth2Client, and go through the OAuth2 content
     * workflow.  Return the full client to the callback.
     */
    // function getAuthenticatedClient() {
    //   return new Promise((resolve, reject) => {
    //     // create an oAuth client to authorize the API call.  Secrets are kept in a `keys.json` file,
    //     // which should be downloaded from the Google Developers Console.


    //     // Generate the url that will be used for the consent dialog.
    //     const authorizeUrl = oAuth2Client.generateAuthUrl({
    //       access_type: 'offline',
    //       scope: 'https://www.googleapis.com/auth/drive.file',
    //       prompt: 'consent'
    //     });

    //     // Open an http server to accept the oauth callback. In this simple example, the
    //     // only request to our webserver is to /oauth2callback?code=<code>
        // const server = http.createServer(async (req, res) => {
        //   if (req.url.indexOf('/oauth2callback') > -1) {
        //     // acquire the code from the querystring, and close the web server.
        //     const qs = querystring.parse(url.parse(req.url).query);
        //     console.log(`Code is ${qs.code}`);
        //     res.end('Authentication successful! Please return to the console.');
        //     server.close();

        //     // Now that we have the code, use that to acquire tokens.
        //     const r = await oAuth2Client.getToken(qs.code);
        //     // Make sure to set the credentials on the OAuth2 client.
        //     oAuth2Client.setCredentials(r.tokens);
        //     console.info('Tokens acquired.');
        //     console.log(r.tokens);
        //     // resolve(oAuth2Client);
        //   }
        // }).listen(3000, () => {
        //   // open the browser to the authorize url to start the workflow
        //   opn(authorizeUrl);
        // });
    //   });
    // }
    // main();



})



router.post("/google-auth-login",function (req, res) {
  console.log("Request=",req)


});
// .then( (obj) => {
            //     //create a sheet
            //     const resource = {
            //         properties: {
            //           title,
            //         },
            //       };
            //       this.sheetsService.spreadsheets.create({
            //         resource,
            //         fields: 'spreadsheetId',
            //       }, (err, spreadsheet) =>{
            //         if (err) {
            //           // Handle error.
            //           console.log(err);
            //         } else {
            //           console.log(`Spreadsheet ID: ${spreadsheet.spreadsheetId}`);
            //         }
            //       });
            // })
            // Once we have the access and refresh token
            // we need to substantiate the auto refresh or the refresh token

router.get('/logged-in-status', function(req,res) {
    console.log('checking to see if user is already logged-in');
    if(req.cookies.token) {
        knex('sessions').where('unique_token', req.cookies.token).first()
        .then((session)=> {
            console.log('session object', session);
            // console.log(session.session_start_time)
            console.log(Date.now()+604800000)
            if(session) {
                if(session.session_start_time < Date.now()+604800000 ){
                    console.log('user is logged in');
                    res.status(200).send({message: 'logged in'});
                }
            }
            else {
                console.log('user is not logged in')
                res.status(200).send({message: 'not logged in'})
            }
        })
        .catch((error)=> {
            res.status(500).send(error);
            console.log(error);
        })
    }
    else {
        res.status(200).send({message: 'not logged in'})
    }
})

router.get('/logout', function(req,res) {
    console.log('finding user session');
    knex('sessions').where('unique_token', req.cookies.token).first().del()
    .then(()=> {
       res.status(200).send({message: 'successfully logged out'})
       console.log('successfully logged out response sent after removing session from database');
    })
    .catch((error)=> {
        res.status(500).send(error);
        console.log(error);
    })
})

router.post('/', function(req, res) {
    console.log('hit user create account route');
    knex('users').where('email', req.body.email).first()
    .then( (user)=> {
        console.log("checked database for user")
        console.log(user);
        if (!user) {
            console.log('user does not alread exist');
            return bcrypt.hash(req.body.password, 12).then( (hashed_password) => {
                console.log('adding user to database with hashed password');
                return knex('users').returning("user_id").insert({
                email: req.body.email,
                hashed_password: hashed_password,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                direct_phone: req.body.direct_phone,
                title_role: req.body.title,
                })
                .then((user_id)=> {
                    console.log("-------->>>>>>>> Successfully added user and returned user_id: ", user_id)
                    return knex('companies').returning("company_id").insert({
                        company_name: req.body.company_name,
                        company_phone: req.body.company_phone,
                        company_website: req.body.website,
                        })
                        .then((company_id)=> {
                            console.log("-------->>>>>>>> Successfully added company and returned company_id: ", company_id)
                            console.log("adding relations. CompanyID:",company_id,"user_id: ", user_id )
                            return knex('company_user_permissions').insert({
                                company_id: parseInt(company_id),
                                user_id: parseInt(user_id),
                                })
                                .then(()=> {
                                    console.log("-------->>>>>>>> Successfully added company_user relation. Now retreving full user record to add cookie and send email, user_id:", user_id)
                                    return knex('users').where('user_id', parseInt(user_id))
                                    .then((user_graph)=> {
                                        console.log("retreived user record with email:",user_graph)
                                        let token = uuidv4();
                                        console.log("-------->>>>>>>> Adding Session to Database for, user_id:", user_id)
                                        return knex('sessions').insert( {
                                            user_id: user_id,
                                            unique_token: token
                                        })
                                        .then( () => {
                                            console.log("-------->>>>>>>> Successfully added cookie token to session table. Now going to send email")
                                            if(process.env.NODE_ENV) {
                                                let url = 'https://statelesskey.herokuapp.com/third-party-service/mailgun/confirm-email-production'
                                                axios.post(url, {user_id: user_graph.user_id, email: user_graph.email, app_name: "Field Wheel Consulting"})
                                                .then(response => {
                                                    if (response.data.message === 'success') {
                                                        let returnObj = {message: 'success'};
                                                        res.cookie('token', token, {httpOnly: true}).send(returnObj);
                                                    }
                                                })
                                                .catch((error) => {
                                                    res.status(500).send(error);
                                                });
                                            }
                                            else {
                                                console.log("Entered appropriate else")
                                                let url = 'https://statelesskey.herokuapp.com/third-party-service/mailgun/confirm-email-development'
                                                axios.post(url, {user_id: user_graph.user_id, email: user_graph.email, app_name: "Field Wheel Consulting"})
                                                    .then(response => {
                                                        console.log("received response from email microservice: ", response)
                                                        if (response.data.message === 'success') {
                                                            let returnObj = {message: 'success'};
                                                            console.log("sending successful response to client")
                                                            res.cookie('token', token, {httpOnly: true}).send(returnObj);
                                                        }
                                                        else {
                                                            console.log("Email mircro service error: ", response)
                                                        }
                                                        return
                                                    })
                                                    .catch((error) => {
                                                        res.status(500).send(error);
                                                    });

                                            }
                                        })
                                        .catch((error) => {
                                            res.status(500).send(error);
                                        });
                                    })
                                    .catch((error) => {
                                        console.log("This is where the error occured: ",error)
                                        res.status(500).send(error);
                                    });
                                })
                                .catch((error) => {
                                    console.log("this is where the error occured: ",error)
                                    res.status(500).send(error);
                                });
                        })
                        .catch((error) => {
                            res.status(500).send(error);
                        });
                })
                .catch((error) => {
                    res.status(500).send(error);
                });
            })
            .catch((error) => {
                res.status(500).send(error);
            });
        }
        else {
            let returnObj = {message: "This email is already associated with an account. Please login to your existing account, or create a new account."};
            res.status(200).send(returnObj);
        }
    })
    .catch((error) => {
        res.status(500).send(error);
    });
});

router.post('/resend-email-confirmation', function(req, res) {
    console.log(req.credentials);
    // Cookies that have not been signed
    console.log('Cookies: ', req.cookies.token)
    // let cookieStr = req.cookies;
    // return cookieParser.JSONCookies(cookieStr)
    // .then((jsonCookies)=> {
    //     let token = jsonCookies.token;
    //     console.log(token);
    //     return token
    // })
    // .then((token) => {
    knex('sessions').where('unique_token', req.cookies.token).first()
    .then((session) => {
        console.log(session);
       return knex('users').where('user_id', session.user_id).first()
    })
    .then((user_graph)=> {
        console.log('user_graph:',user_graph)
        if(process.env.NODE_ENV) {
            let url = 'https://statelesskey.herokuapp.com/third-party-service/mailgun/confirm-email-production'
            axios.post(url, {user_id: user_graph.user_id, email: user_graph.email})
            .then(response => {
                if (response.data.message === 'success') {
                    let returnObj = {message: 'success'};
                    res.send(returnObj);
                }
            })
            .catch((error) => {
                res.status(500).send(error);
            });
        }
        else {
            console.log('entered else')
            let url = 'https://statelesskey.herokuapp.com/third-party-service/mailgun/confirm-email-development'
            axios.post(url, {user_id: user_graph.user_id, email: user_graph.email})
            .then(response => {
                if (response.data.message === 'success') {
                    let returnObj = {message: 'success'};
                    res.send(returnObj);
                }
            })
            .catch((error) => {
                res.status(500).send(error);
            });
        }
    })
    .catch((error) => {
        res.status(500).send(error);
    })
})

router.get('/confirm-email/:user_id', function(req, res) {
    console.log(req.params.user_id);
    knex('users').where('user_id', req.params.user_id).first()
    .then((user)=> {
        console.log(user);
        // if(user.email_confirmed === false) {
        //     console.log('evaluates to false');
        // }
        if(!user.email_confirmed) {
            return knex('users').where('user_id', user.user_id).update({
                email_confirmed: true
            }).then((user)=> {
                // console.log(res.cookies);
                if(res.cookies) {
                    let token = res.cookies.token;
                    return knex('sessions').where('unique_token', token).first()
                    .then((session)=> {
                        if (session.user_id === user.user_id) {
                            res.status(200).redirect('/')
                        }
                    })
                    .catch((error)=> {
                        console.log(error);
                        res.status(500).send();
                    })
                }
                else {
                    res.status(200).redirect('/sign-in')
                }
            })
            .catch ((error)=> {
                console.log(error);
                res.status(500).send();
            })
        }
        else if(user.email_confirmed == true) {
            console.log('evaluates to true');
            res.status(200).redirect('/email-previously-confirmed').send({message: "This email was already confirmed. Please login."});
        }
        else {
            res.status(200).send({message: 'the user you tried to confirm is not a valid user, please confirm you are on the appropraite link'})
        }
    })
    .catch((error) => {
        res.status(500).send();
    })
})

router.post("/login", function (req, res) {
    knex('users').where('email', req.body.email).first()
    .then( (user_graph) => {
        console.log(user_graph);
        if(!user_graph) {
            console.log('user_graph is false')
            let returnObj = {message: "email not found"};
            res.cookie('token', 'failed-login-attempt', {httpOnly: true}).status(200).send(returnObj)
        }
        else {
            return bcrypt.compare(req.body.password, user_graph.hashed_password)
            .then(()=> {
                console.log('password matched');
                return knex('users').where('email', req.body.email).first()
            })
            .then( (user_graph) => {
                console.log(user_graph);
                return knex('sessions').where('user_id', user_graph.user_id)
                .then( (session) => {
                    if(session && session.length !== 0) {
                        console.log('found session: ', session);
                        let token = uuidv4();
                        // console.log(user_graph);
                        return knex('sessions')
                            .where('user_id', user_graph.user_id)
                            .update( {
                            unique_token: token
                            })
                            .then( () => {
                                console.log('setting cookie',token)
                                let returnObj = {message: "success"};
                                res.cookie('token', token, {httpOnly: true}).status(200).send(returnObj);
                            })
                            .catch( (error)=> {
                                console.log('error setting cookie')
                                res.status(500).send(error);
                            })
                    }
                    else {
                        console.log('inserting session token');
                        let token = uuidv4();
                        return knex('sessions').insert( {
                            user_id: user_graph.user_id,
                            unique_token: token
                        })
                        .then(() => {
                            console.log('setting cookie', token)
                            let returnObj = {message: "success", token: token};
                            res.cookie('token', token, {httpOnly: true}).status(200).send(returnObj);
                        })
                        .catch( (error)=> {
                            res.status(500).send(error);
                        })
                        }

                    })
            })
            .catch(bcrypt.MISMATCH_ERROR, (error) => {
                res.send({error: "incorrect pasword"});
            })
            .catch((error) => {
                res.status(500).send(error);
            })
        }
    })
    .catch((error) => {
        res.status(500).send(error);
    })
})

router.post('/pw-reset', function(req, res) {
    console.log('request initiated')
    knex('users').where('email', req.body.email).first()
    .then((user_graph)=> {
        console.log(user_graph.user_id);
        let urlToken = uuidv4();
        console.log(urlToken);
        if(process.env.NODE_ENV) {
            let data = {
                from: 'VoiceMachine.io <no-reply@fieldwheel.co>',
                to: user_graph.email,
                subject: 'Resetting Your Password',
                text: 'Please visit https://voicemachineio.herokuapp.com/pw-reset/'+user_graph.user_id+'/'+ urlToken + ' to reset your password. This link will expire in 5 minutes'
            };
            console.log(data);
            mailgun.messages().send(data, function (error, body) {
            });
        }
        else {
            let data = {
                from: 'VoiceMachine.io <no-reply@fieldwheel.co>',
                to: user_graph.email,
                subject: 'Resetting Your Password',
                text: 'Please visit https://localhost:8000/pw-reset/'+user_graph.user_id+'/'+ urlToken + ' to reset your password. This link will expire in 5 minutes'
            };
            console.log(data);
            mailgun.messages().send(data, function (error, body) {
            });
        }
        return bcrypt.hash(urlToken, 12).then((token)=> {
            return knex('pw_reset_log').insert({
                user_id: user_graph.user_id,
                pw_reset_token: token
            })
            .then( ()=> {
                res.status(200).send({email: user_graph.email})
            })
        })
    })
    .catch((error) => {
        res.status(500).send(error);
    })
});

//reset password
router.post('/pw-reset/:user_id/:urlToken', function(req, res) {
    console.log(req.params.user_id);
    knex('pw_reset_log').where('user_id', req.params.user_id).first()
    .then((pw_reset_row)=> {
        if (pw_reset_row) {
            return bcrypt.compare(req.body.password, pw_reset_row.hashed_password)
            .then(()=> {
                if((Date.now() - pw_reset_row.password_reset) > 3600000 ) {
                    res.status(200).send({message: "The reset link you've requested has expired. Please request a new reset link."})
                }
                else {
                    return knex('sessions').where('user_id', user.user_id)
                        .then( (session) => {
                            if(session) {
                                console.log('found session: ', session);
                                let token = uuidv4();
                                // console.log(user_graph);
                                return knex('sessions')
                                    .where('user_id', user.user_id)
                                    .update( {
                                    unique_token: token
                                    })
                                    .then( () => {
                                        console.log('setting cookie',token)
                                        let returnObj = {message: "success, ready to change password."};
                                        res.cookie('token', token, {httpOnly: true}).status(200).send(returnObj).redirect('pw-reset');
                                    })
                                    .catch( (error)=> {
                                        console.log('error setting cookie')
                                        res.status(500).send(error);
                                    })
                            }
                            else {
                                console.log('inserting session token');
                                let token = uuidv4();
                                return knex('sessions').insert( {
                                    user_id: user.user_id,
                                    unique_token: token
                                })
                                .then(() => {
                                    console.log('setting cookie', token)
                                    let returnObj = {message: "success, ready to change password", token: token};
                                    res.cookie('token', token, {httpOnly: true}).status(200).send(returnObj).redirect('/reset-pw');
                                })
                                .catch( (error)=> {
                                    res.status(500).send(error);
                                })
                            }
                        })
                        .catch(bcrypt.MISMATCH_ERROR, (error) => {
                            res.send({error: "invalid authorization"});
                            console.log(error);
                        })
                }
            })
            .catch((error)=> {
                res.status(500).send(error);
                console.log('error when chaining .then from user_pw_reset lookup. Data: ', error);
            })
        }
        else {
            res.status(200).send({message: "invalid user"})
        }
    })
    .catch((error) => {
        res.status(500).send(error);
    })
});

router.post('/resetting-password/', function(req, res) {
    let token = req.cookies.token;
    knex('sessions').where('unique_token', token).first()
    .then((session) => {
        let user_id = session.user_id
        return knex('users').where('user_id', user_id).first();
    })
    .then( (user)=> {
        console.log('reseting password for user:',user);
        if((Date.now() - user.password_reset) > 3600000 ) {
            res.status(200).send({message:"The reset link you've requested has expired. Please request a new reset link."})
        }
        else {
            return bcrypt.hash(req.body.password, 12).then( (hashed_password) => {
                return knex('users').where('user_id', req.params.user_id).first().update( {
                    password: hashed_password
                })
            })
            .catch((error)=> {
                res.status(500).send(error);
                console.log(error);
            })
        }
    })
    .catch((error)=> {
        res.status(500).send(error);
        console.log(error);
    })
});

router.get('/my-account/', function(req, res) {
    let token = req.cookies.token;
    knex('sessions').where('unique_token', token).first()
    .then((session) => {
        console.log('my account session found:', session)
        let user_id = session.user_id
        return knex('users').where('user_id', user_id).first();
    })
    .then( (user)=> {
        let resObj = {};
        resObj.email = user.email;
        // return knex('numbers').where('user_id', user_id)
        // .then((numbers)=> {
        res.status(200).send(resObj);
        // })
    })
    .catch((error)=> {
        res.status(500).send(error);
        console.log(error);
    })
});


router.delete('/', function(req, res) {
    console.log('request initiated');
    let token = req.cookies.token;
    knex('sessions').where('unique_token', token).first()
    .then((session)=> {
        knex('calls').where('user_id', req.body.session.user_id).del()
        .then(()=> {
            knex('record_attatchments_emails').where('calls', req.body.session.user_id).del()
        })

    })
});

module.exports = router;
