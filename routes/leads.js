'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */


// const http = require('http');
const url = require('url');
const querystring = require('querystring');
// const opn = require('opn');

//Setting up knex
const env = process.env.NODE_ENV ||'development';
const config = require('../knexfile')[env];
const knex = require('knex')(config);

const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');

const axios = require('axios');

router.put("/", function (req, res) {
  console.log('checking to see if user is already logged-in');
  console.log(req.cookies.token);
  console.log("initial req.body=",req.body)
  axios.get(`http://localhost:${process.env.PORT || 8000}/auth/${req.cookies.token}`)
  .then(function (resp) {
    console.log("axios res=",resp.data);
    if(resp.data.message == 'logged in') {
      console.log("log in auth complete");
      // console.log(req.body)
      delete req.body.spouse;
      console.log("req.body=",req.body);
      return knex('leads').update(req.body).where({'lead_id':req.body.lead_id}).returning(['lead_id','lead_status'])
      .then((data)=> {
        console.log("data=",data);
        res.status(200).send({message: 'success',lead_id:data[0],lead_status:data[0],cordinates:req.body.cordinates})
        console.log("update of lead complete setting apt")
      })
    }
    else {
      res.status(200).send({message: 'not logged in'});
    }
  })
  .catch(function (error) {
    console.log("axios error=",error);
  })
})

router.post("/", function (req, res) {
  console.log('checking to see if user is already logged-in');
  console.log(req.cookies.token);
  axios.get(`http://localhost:${process.env.PORT || 8000}/auth/${req.cookies.token}`)
  .then(function (resp) {
    console.log("axios res=",resp.data);
    if(resp.data.message == 'logged in') {
      console.log("log in auth complete");
      console.log(req.body)
      delete req.body.spouse;
      return knex('leads').insert(req.body).returning(['lead_id','lead_status'])
      .then((data)=> {
        console.log("data=",data);
        res.status(200).send({message: 'success',lead_id:data[0].lead_id,lead_status:data[0].lead_status})
        console.log("insert of lead complete setting apt")
      })
    }
    else {
      res.status(200).send({message: 'not logged in'});
    }
  })
  .catch(function (error) {
    console.log("axios error=",error);
  })
})

router.post("/power-bill-upload", function (req, res) {
  console.log('checking to see if user is already logged-in');
  console.log(req.body);
})

router.get("/", function (req, res) {
  console.log('checking to see if user is already logged-in');
  console.log(req.cookies.token);
  axios.get(`http://localhost:${process.env.PORT || 8000}/auth/${req.cookies.token}`)
  .then(function (resp) {
    console.log("axios res=",resp.data);
    if(resp.data.message == 'logged in') {
      console.log("log in auth complete");
      return knex('leads')
      .then((data)=> {
        console.log("retreived leads from database:",data);
        res.status(200).send(data)
      })
    }
    else {
      res.status(200).send({message: 'not logged in'});
    }
  })
  .catch(function (error) {
    console.log("axios error=",error);
  })
})


module.exports = router;
