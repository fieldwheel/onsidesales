module.exports = {

development: {
client: 'pg',
connection: 'postgres://localhost/OnSideSales'
},

production: {
client: 'postgresql',
connection: process.env.DATABASE_URL
}

};
