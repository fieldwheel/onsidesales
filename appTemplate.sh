#!/bin/bash

mkdir "static"

mkdir "static/js-framework"

touch "static/js-framework/globalObj.js"

touch "static/js-framework/validations.js"

touch "static/js-framework/framework_methods.js"

mkdir "static/js-framework/event_listeners"

touch "static/js-framework/event_listeners/event_listeners.js"



touch "static/js-framework/router.js"

mkdir "static/js-framework/state_functions"

touch "static/js-framework/state_functions/state_functions.js"

mkdir 'static/styles'

mkdir 'static/styles/global.css'

mkdir 'static/styles/home.css'

touch 'static/index.html'

touch 'server.js'

mkdir "routes"

mkdir "routes/state_routes"

touch 'routes/state_routes/index.js'

cat << 'EOF' > routes/state_routes/index.js
'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */

router.get("/", function (req, res) {
    console.log('/ route hit');
    console.log(req.cookies);
    res.sendFile('index.html', {'root': 'static/'})
})

module.exports = router;
EOF

cat << 'EOF' > routes/user.js
'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */

const querystring = require('querystring');

//Setting up knex
const env = process.env.NODE_ENV ||'development';
const config = require('../knexfile')[env];
const knex = require('knex')(config);

const bcrypt = require('bcrypt');
const uuidv4= require('uuid/v4');

const axios = require('axios');



module.exports = router;
EOF



cat << 'EOF' > static/index.html
<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" media="screen" href="styles/global.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="styles/home.css" />
  </head>
  <body>
    <h1>Home</h1>
    <script src="js-framework/globalObj.js"></script>
    <script src="js-framework/framework_methods.js"></script>
    <script src="js-framework/event_listeners/event_listeners.js"></script>
    <script src="js-framework/state_functions/state_functions.js"></script>
    <script src="js-framework/router.js"></script>
  </body>
</html>
EOF

cat << 'EOF' > server.js
const express = require('express');
const app = express();

// const usersPath = path.join(__dirname, 'postgres://localhost/name_of_database');


if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

// Handle incoming JSON objects and form data
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
  }));
app.use(bodyParser.json());

const env = process.env.NODE_ENV || 'development';
const config = require('./knexfile')[env];
const knex = require('knex')(config);
const cookieParser = require('cookie-parser');
app.use(cookieParser());

app.disable('x-powered-by');

// Uses the public folder as our station for static front-end files
app.use(express.static('static'));

//application routes
//const user = require('./routes/user');
//app.use('/user', user);


//state_routes

//const indexRouter = require('./routes/state_routes/index');
//app.use('/', indexRouter);

const port = process.env.PORT || 8000;
app.listen(port, function() {
    console.log('Listening on port', port);
});

module.exports = app;
EOF




npm init -y

touch .gitignore

touch .env

npm install --save pg knex body-parser querystring express bcrypt dotenv cookie-parser uuidv4

npm install -g knex

knex init

npm install nodemon -g

cat << 'EOF' > knexfile.js
module.exports = {

development: {
client: 'pg',
connection: 'postgres://localhost/name_of_database'
},

production: {
client: 'postgresql',
connection: process.env.DATABASE_URL
}

};
EOF

nodemon server.js

echo "App configuration successful. You may now open you browser to localhost:8000 and start coding"
